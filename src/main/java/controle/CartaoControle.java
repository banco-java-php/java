package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import modelo.ContaBancaria;
import modelo.Cartao;

public class CartaoControle {
	public boolean adicionarCartao(Cartao car) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "INSERT INTO Cartao(id,limite,id_usuario,id_conta, valor) VALUES (?,?,?,?,?)";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setInt(1, car.getId());
			ps.setInt(3,car.getUsuario().getId());
			if(car.getLimite() != 0) {
				ps.setDouble(2, car.getLimite());
				ps.setDouble(5, car.getLimite());
			}else {
				ps.setNull(2, Types.INTEGER);
				ps.setDouble(5, 0);
			}
			if(car.getConta().getConta() != 0) {
				ps.setInt(4,car.getConta().getConta());
			}else {
				ps.setNull(4, Types.INTEGER);
			}
			if(!ps.execute()) {
				resultado = true;
			}
			
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra 1 - CartaoControle");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra 2 - CartaoControle");
		}
		return resultado;
	}
	public ArrayList<Cartao> consultarCartoes(int id){
		ArrayList<Cartao> car = new ArrayList<Cartao>();
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "SELECT * FROM Cartao WHERE id_usuario=?";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					Cartao cartao = new Cartao();
					cartao.setId(rs.getInt("id"));
					cartao.setLimite(rs.getDouble("limite"));
					cartao.setValor(rs.getDouble("valor"));
					cartao.getUsuario().setId(rs.getInt("id_usuario"));
					if(rs.getInt("id_conta") != 0) {
						ContaBancaria cb = new ContaBancariaControle().consultarContaBancaria(rs.getInt("id_conta"));
						cartao.setConta(cb);
					}
					car.add(cartao);
				}
			}
			
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra 1 - CartaoControle");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra 2 - CartaoControle");
		}
		return car;
	}
	
	public Cartao consultarCartoesById(int id) {
		Cartao cartao = null;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM Cartao WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null && rs.next()) {
				cartao = new Cartao();
				cartao.setId(rs.getInt("id"));
				cartao.setLimite(rs.getDouble("limite"));
				cartao.setValor(rs.getDouble("valor"));
				cartao.getUsuario().setId(rs.getInt("id_usuario"));
				if(rs.getInt("id_conta") != 0) {
					ContaBancaria cb = new ContaBancariaControle().consultarContaBancaria(rs.getInt("id_conta"));
					cartao.setConta(cb);
				}
			
			}
			
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - Cartao Controle");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - Cartao Controle");
		}
		return cartao;
	}
	
	public boolean atualizarValor(int id,double valor) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "UPDATE Cartao SET valor=valor + ? WHERE id=?";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setDouble(1, valor);
			ps.setInt(2, id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - Cartao Controle");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - Cartao Controle");
		}
		return resultado;
	}
	public boolean deletar(int cartao) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "UPDATE Registro SET id_cartao=NULL WHERE id_cartao=?;";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setInt(1, cartao);
			boolean a = ps.execute();
			String sql3 = "DELETE FROM Cartao WHERE id=?";
			PreparedStatement ps3 = connect.prepareStatement(sql3);
			ps3.setInt(1, cartao);
			boolean c = ps3.execute();
			if(!a && !c) {
				resultado = true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - Cartao Controle");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - Cartao Controle");
		}
		return resultado;
	}
	
}
