package controle;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.Usuario;
import java.util.ArrayList;

public final class UsuarioControle {
	public boolean insert(Usuario user) {
		boolean resultado=false;
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="INSERT INTO Usuario(nome,senha, email, carteira) VALUES(?,?,?,?)";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setString(1, user.getNome());
			ps.setString(2, user.getSenha());
			ps.setString(3, user.getEmail());
			ps.setDouble(4, 0.00);
			if(!ps.execute()) {
				resultado=true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public ArrayList<Usuario> listarUsuarios(){
		ArrayList<Usuario> list=null;
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="SELECT * FROM Usuario;";
			PreparedStatement ps= connect.prepareStatement(sql);
			ResultSet rs= ps.executeQuery();
			if(rs!=null) {
				list=new ArrayList<Usuario>();
				while(rs.next()) {
					Usuario usr= new Usuario();
					usr.setId(rs.getInt("id"));
					usr.setNome(rs.getString("nome"));
					usr.setSenha(rs.getString("senha"));
					list.add(usr);
				}
				new Conexao().fecharConexao(connect);
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return list;
	}
	public boolean verificarEmail(String email) {
		boolean resultado = false;
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="SELECT email FROM Usuario WHERE email = ?";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			if(rs != null && rs.next()) {
				resultado = true;
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public Usuario verificar(Usuario user) {
		Usuario resultado= null;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql="SELECT * FROM Usuario WHERE email=? and senha=?;";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setString(1, user.getEmail());
			ps.setString(2, user.getSenha());
			ResultSet rs= ps.executeQuery();
			if(rs.next()){
				resultado = new Usuario();
				resultado.setId(rs.getInt("id"));
				resultado.setNome(rs.getString("nome"));
				resultado.setEmail(rs.getString("email"));
				resultado.setCarteira(rs.getDouble("carteira"));
			}
			new Conexao().fecharConexao(connect);
		}catch(Exception e) {
			System.out.println(e.getMessage()+"errorrrrr");
		}
		return resultado;
	}
	public boolean adicionarCarteira(Double carteira, int id) {
		boolean resultado=false;
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="UPDATE Usuario SET carteira=? WHERE id=?";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setDouble(1, carteira);
			ps.setDouble(2, id);
			if(!ps.execute()) {
				resultado=true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	
	public boolean atualizarCarteira(double carteira, int id) {
		boolean resultado=false;
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="UPDATE Usuario SET carteira=carteira+ ? WHERE id=?";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setDouble(1, carteira);
			ps.setInt(2, id);
			if(!ps.execute()) {
				resultado=true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	
	public Usuario mostrarCarteira(int id) {
		Usuario usu = new Usuario();
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="SELECT carteira FROM Usuario WHERE id = ?";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				usu.setCarteira(rs.getDouble("carteira"));
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return usu;
	}
}
