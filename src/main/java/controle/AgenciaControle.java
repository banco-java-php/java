package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Agencia;

public class AgenciaControle {
	public Agencia consultarAgencia(int id){
		Agencia age = new Agencia();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM Agencia WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					age.setId(rs.getInt("id"));
					age.setNome(rs.getString("nome"));
				}
			}
			
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - AgenciaControle consultarAgencia 1");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - AgenciaControle consultarAgencia 2");
		}
		return age;
	}
}
