package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modelo.ContaBancaria;
import modelo.Agencia;
import modelo.Usuario;

public class ContaBancariaControle {
	public boolean adicionarContaBancaria(ContaBancaria con,int id_usuario,int id_agencia) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "INSERT INTO ContaBancaria(numeroConta,valor,tipoConta,id_usuario,id_agencia) VALUES (?,?,?,?,?)";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setInt(1, con.getConta());
			ps.setDouble(2, con.getValor());
			ps.setString(3, con.getTipoConta());
			ps.setInt(4,id_usuario);
			ps.setInt(5,id_agencia);
			if(!ps.execute()) {
				resultado = true;
			}
			
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra 1 - ContaBancariaControle");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra 2 - ContaBancariaControle");
		}
		return resultado;
	}
	
	public ArrayList<ContaBancaria> consultarContasBancarias(int id) {
		ArrayList<ContaBancaria> lista = new ArrayList<ContaBancaria>();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM ContaBancaria WHERE id_usuario=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					ContaBancaria conta = new ContaBancaria();
					conta.setConta(rs.getInt("numeroConta"));
					conta.setValor(rs.getDouble("valor"));
					conta.setTipoConta(rs.getString("tipoConta"));
					Usuario usu = new Usuario();
					usu.setId(id);
					conta.setUsuario(usu);
					AgenciaControle agC = new AgenciaControle();
					Agencia ag = agC.consultarAgencia(rs.getInt("id_agencia"));
					conta.getAgencia().setNome(ag.getNome());
					lista.add(conta);
				}
			}
			
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - Conta bancaria");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - Conta bancaria");
		}
		return lista;
	}
	public ContaBancaria consultarContaBancaria(int id) {
		ContaBancaria conta = null;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM ContaBancaria WHERE numeroConta=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					conta = new ContaBancaria();
					conta.setConta(rs.getInt("numeroConta"));
					conta.setValor(rs.getDouble("valor"));
					conta.setTipoConta(rs.getString("tipoConta"));
					Usuario usu = new Usuario();
					usu.setId(id);
					conta.setUsuario(usu);
					AgenciaControle agC = new AgenciaControle();
					Agencia ag = agC.consultarAgencia(rs.getInt("id_agencia"));
					conta.setAgencia(ag);
				}
			}
			
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - Conta bancaria");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - Conta bancaria");
		}
		return conta;
	}
	public boolean atualizarValor(int id,double valor) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "UPDATE ContaBancaria SET valor=valor + ? WHERE numeroConta=?";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setDouble(1, valor);
			ps.setInt(2, id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - Conta bancaria aqui");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - Conta bancaria");
		}
		return resultado;
	}
	public boolean deletar(int conta) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			
			String sql = "UPDATE Registro SET id_conta=NULL WHERE id_conta=?;";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setInt(1, conta);
			boolean a = ps.execute();
			String sql2 = "UPDATE Cartao SET id_conta=NULL WHERE id_conta=?;";
			PreparedStatement ps2 = connect.prepareStatement(sql2);
			ps2.setInt(1, conta);
			boolean b = ps2.execute();
			String sql3 = "DELETE FROM ContaBancaria WHERE numeroConta=?";
			PreparedStatement ps3 = connect.prepareStatement(sql3);
			ps3.setInt(1, conta);
			boolean c = ps3.execute();
			if(!a && !b && !c) {
				resultado = true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - Conta bancaria");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - Conta bancaria");
		}
		return resultado;
	}
}
