package controle;

import modelo.Registro;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import controle.Conexao;
import controle.ContaBancariaControle;
import controle.CartaoControle;
import modelo.Usuario;
import modelo.ContaBancaria;
import modelo.Cartao;

public class RegistroControle {
	public boolean adicionarRegistro(Registro reg,Usuario id,String forma,int id_Forma) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "INSERT INTO Registro(titulo,valor,descricao,repeticao,tipoCartao,tipoPagamento,tipoRegistro,dataRegistro,dataOperacao,dataVencimento,id_cartao,id_conta,id_usuario) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement ps = connect.prepareStatement(sql);
			
			int idU=id.getId();
			
			ps.setString(1, reg.getTitulo());
			ps.setDouble(2, reg.getValor());
			ps.setString(3, reg.getDescricao());
			ps.setString(4, reg.getRepeticao());
			ps.setString(5, reg.getTipoCartao());
			ps.setString(6, reg.getTipoPagamento());
			ps.setString(7, reg.getTipoRegistro());
			ps.setString(8, reg.getDataRegistro());
			ps.setString(9, reg.getDataOperacao());
			ps.setString(10, reg.getDataVencimento());
			
			if(forma.equals("conta")) {
				ps.setInt(12, id_Forma);
				ps.setNull(11, Types.INTEGER);
			}else if(forma.equals("cartao")){
				ps.setInt(11, id_Forma);
				ps.setNull(12, Types.INTEGER);
			}else {
				ps.setNull(11, Types.INTEGER);
				ps.setNull(12, Types.INTEGER);
			}
			ps.setInt(13, idU);
			if(!ps.execute()) {
				resultado = true;
			}
			
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra 1 - RegistroControle");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra 2 - RegistroControle");
		}
		return resultado;
	}
	
	public ArrayList<Registro> consultarRegistros(int id) {
		ArrayList<Registro> lista = new ArrayList<Registro>();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM Registro WHERE id_usuario="+id+" ORDER BY dataRegistro DESC";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					Registro reg = new Registro();
					reg.setId(rs.getInt("id"));
					reg.setTitulo(rs.getString("titulo"));
					reg.setValor(rs.getDouble("valor"));
					reg.setDescricao(rs.getString("descricao"));
					reg.setRepeticao(rs.getString("repeticao"));
					reg.setTipoCartao(rs.getString("tipoCartao"));
					reg.setTipoPagamento(rs.getString("tipoPagamento"));
					reg.setTipoRegistro(rs.getString("tipoRegistro"));
					reg.setDataRegistro(rs.getString("dataRegistro"));
					reg.setDataOperacao(rs.getString("dataOperacao"));
					reg.setDataVencimento(rs.getString("dataVencimento"));
					if(rs.getInt("id_conta")!=0){
						ContaBancariaControle contaC = new ContaBancariaControle();
						ContaBancaria conta = contaC.consultarContaBancaria(rs.getInt("id_conta"));
						reg.setConta(conta);
					}
					if(rs.getInt("id_cartao")!=0){
						CartaoControle cartaoC = new CartaoControle();
						Cartao cartao = cartaoC.consultarCartoesById(rs.getInt("id_cartao"));
						reg.setCartao(cartao);
					}
					
					
					lista.add(reg);
				}
			}
			
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+":");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - RegistroControle consultarRegistros 2");
		}
		return lista;
	}
	
	public ArrayList<Registro> consultarRegistrosFiltro(int id,String filtro,String valor) {
		ArrayList<Registro> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM Registro WHERE id_usuario="+id+" and "+filtro+"  LIKE '%"+valor+"%' ORDER BY dataRegistro DESC";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Registro>();
				while(rs.next()) {
					Registro reg = new Registro();
					reg.setId(rs.getInt("id"));
					reg.setTitulo(rs.getString("titulo"));
					reg.setValor(rs.getDouble("valor"));
					reg.setDescricao(rs.getString("descricao"));
					reg.setRepeticao(rs.getString("repeticao"));
					reg.setTipoCartao(rs.getString("tipoCartao"));
					reg.setTipoPagamento(rs.getString("tipoPagamento"));
					reg.setTipoRegistro(rs.getString("tipoRegistro"));
					reg.setDataRegistro(rs.getString("dataRegistro"));
					reg.setDataOperacao(rs.getString("dataOperacao"));
					reg.setDataVencimento(rs.getString("dataVencimento"));
					if(rs.getInt("id_conta")!=0){
						ContaBancariaControle contaC = new ContaBancariaControle();
						ContaBancaria conta = contaC.consultarContaBancaria(rs.getInt("id_conta"));
						reg.setConta(conta);
					}
					if(rs.getInt("id_cartao")!=0){
						CartaoControle cartaoC = new CartaoControle();
						Cartao cartao = cartaoC.consultarCartoesById(rs.getInt("id_cartao"));
						reg.setCartao(cartao);
					}
					lista.add(reg);
				}
			}
			
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - RegistroControle consultarRegistrosLimite 1");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - RegistroControle consultarRegistrosLimite 2");
		}
		return lista;
	}
	
	public Registro consultarRegistrosById(int id) {
		Registro reg = new Registro();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM Registro WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					reg.setId(rs.getInt("id"));
					reg.setTitulo(rs.getString("titulo"));
					reg.setValor(rs.getDouble("valor"));
					reg.setDescricao(rs.getString("descricao"));
					reg.setRepeticao(rs.getString("repeticao"));
					reg.setTipoCartao(rs.getString("tipoCartao"));
					reg.setTipoPagamento(rs.getString("tipoPagamento"));
					reg.setTipoRegistro(rs.getString("tipoRegistro"));
					reg.setDataRegistro(rs.getString("dataRegistro"));
					reg.setDataOperacao(rs.getString("dataOperacao"));
					reg.setDataVencimento(rs.getString("dataVencimento"));
					if(rs.getInt("id_conta")!=0){
						ContaBancariaControle contaC = new ContaBancariaControle();
						ContaBancaria conta = contaC.consultarContaBancaria(rs.getInt("id_conta"));
						reg.setConta(conta);
					}
					if(rs.getInt("id_cartao")!=0){
						CartaoControle cartaoC = new CartaoControle();
						Cartao cartao = cartaoC.consultarCartoesById(rs.getInt("id_cartao"));
						reg.setCartao(cartao);
					}
					
				}
			}
			
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - RegistroControle consultarRegistrosLimite 1");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - RegistroControle consultarRegistrosLimite 2");
		}
		return reg;
	}
	
	public boolean atualizarVencimento(int id) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "UPDATE Registro SET dataVencimento=NULL WHERE id=?";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setDouble(1, id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - RegistroControle consultarRegistros vrnc 1");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - RegistroControle consultarRegistros 2");
		}
		return resultado;
	}
	
	public boolean deletar(int registro, String tipoPagamento, Double valor, int id_forma) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "DELETE FROM Registro WHERE id=?;";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setInt(1, registro);
			boolean a = ps.execute();
			
			valor = valor*(-1);
			String k ="";
			String l = "";
			String m = "";
			switch(tipoPagamento) {
				case "carteira":
					k ="Usuario";
					l = "carteira";
					m = "id";
					break;
				case "conta":
					k ="ContaBancaria";
					l = "valor";
					m = "numeroConta";
					break;
				case "cartao":
					k ="Cartao";
					l = "valor";
					m = "id";
					break;
			}
			
			String sql2 = "UPDATE "+k+" SET "+l+"="+l+"+"+valor+" WHERE "+m+"="+id_forma;
			PreparedStatement ps2 = connect.prepareStatement(sql2);
			
			boolean b = ps2.execute();
			if(!a && !b) {
				resultado = true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage()+"Eta porra - RegistroControle consultarRegistros 1 atu");
		}catch(Exception e) {
			System.out.println(e.getMessage()+"Eta porra - RegistroControle consultarRegistros 2");
		}
		return resultado;
	}
	
}
