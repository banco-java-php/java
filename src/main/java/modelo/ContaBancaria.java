package modelo;
import modelo.Agencia;
public class ContaBancaria {
	private int conta;
	private Usuario usuario = new Usuario();
	private double valor;
	private Agencia agencia = new Agencia();
	private String tipoConta;
	
	public int getConta() {
		return this.conta;
	}
	public void setConta(int conta) {
		this.conta = conta;
	}
	public Usuario getUsuario() {
		return this.usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Agencia getAgencia() {
		return this.agencia;
	}
	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}
	public double getValor() {
		return this.valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getTipoConta() {
		return this.tipoConta;
	}
	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}
	
}
