package modelo;
import modelo.ContaBancaria;
import modelo.Usuario;
public class Cartao {
	private int id;
	private ContaBancaria conta = new ContaBancaria();
	private Usuario usuario = new Usuario();
	private String tipoCartao;
	private double limite;
	private double valor;
	
	public double getValor() {
		return this.valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipoCartao() {
		return this.tipoCartao;
	}
	public void setTipoCartao(String tipoCartao) {
		this.tipoCartao = tipoCartao;
	}
	public double getLimite() {
		return this.limite;
	}
	public void setLimite(double limite) {
		this.limite = limite;
	}
	public ContaBancaria getConta() {
		return this.conta;
	}
	public void setConta(ContaBancaria conta) {
		this.conta = conta;
	}
	public Usuario getUsuario() {
		return this.usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
}
