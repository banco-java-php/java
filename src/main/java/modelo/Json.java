package modelo;

public final class Json {
	/**
	 * m: mensagem
	 * campo: nome do campo no html
	 * tipo: success ou error
	 * **/
	public Json(String m, String tipo,String campo) {
		this.setMensagem(m);
		this.setCampo(campo);
		switch(tipo) {
		case "success":
			this.setCodigo(1);
			break;
		case "error":
			this.setCodigo(0);
			break;
		}
	}
	public Json(String m, String tipo) {
		this.setMensagem(m);
		switch(tipo) {
		case "success":
			this.setCodigo(1);
			break;
		case "error":
			this.setCodigo(0);
			break;
		}
	}
	private String mensagem;
	private String tipo;
	private String campo;
	private int codigo;
	
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public  String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
}
