package modelo;

public class Registro {
	private int id;
	private String titulo;
	private double valor;
	private String descricao;
	private String repeticao;
	private String tipoCartao;
	private String tipoPagamento;
	private String tipoRegistro;
	private String dataRegistro;
	private String dataOperacao;
	private String dataVencimento;
	private Cartao cartao = new Cartao();
	private ContaBancaria conta = new ContaBancaria();
	public ContaBancaria getConta() {
		return this.conta;
	}
	public void setConta(ContaBancaria conta) {
		this.conta = conta;
	}
	private Usuario usuario = new Usuario();
	public Usuario getUsuario() {
		return this.usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return this.titulo;
	}
	public String getRepeticao() {
		return this.repeticao;
	}
	public void setRepeticao(String repeticao) {
		this.repeticao = repeticao;
	}
	public String getTipoPagamento() {
		return this.tipoPagamento;
	}
	public void setTipoPagamento(String tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public double getValor() {
		return this.valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getDescricao() {
		return this.descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getTipoCartao() {
		return this.tipoCartao;
	}
	public void setTipoCartao(String tipoCartao) {
		this.tipoCartao = tipoCartao;
	}
	public String getTipoRegistro() {
		return this.tipoRegistro;
	}
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	public String getDataRegistro() {
		return this.dataRegistro;
	}
	public void setDataRegistro(String dataRegistro) {
		this.dataRegistro = dataRegistro;
	}
	public String getDataOperacao() {
		return this.dataOperacao;
	}
	public void setDataOperacao(String dataOperacao) {
		this.dataOperacao = dataOperacao;
	}
	public String getDataVencimento() {
		return this.dataVencimento;
	}
	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public Cartao getCartao() {
		return this.cartao;
	}
	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}

}
