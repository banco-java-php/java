package modelo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Usuario {
	private int id;
	private String nome;
	private String senha;
	private String email;
	private Double carteira;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = (nome.equals("") && nome.length() < 5) ? "false" : nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		//validar email
		Matcher matcher = Pattern.compile("^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,6}$", Pattern.CASE_INSENSITIVE).matcher(email);
		this.email = (matcher.find()) ? email : "false";
	}
	public Double getCarteira() {
		return carteira;
	}
	public void setCarteira(Double carteira) {
		this.carteira = carteira;
	}
	
}
