package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Json;
import modelo.Usuario;

import com.google.gson.Gson;

import controle.UsuarioControle;
@WebServlet("/carteira")
public class Carteira extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Carteira() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Usuario session = (Usuario)request.getSession().getAttribute("usuario");
		Double carteira = Double.parseDouble(request.getParameter("carteira"));
		UsuarioControle usuario = new UsuarioControle();
		if(carteira > 0){
				carteira = session.getCarteira() + carteira;
				if(usuario.adicionarCarteira(carteira, session.getId())) {
					response.setContentType("application/json; charset=UTF-8"); 
					Json json = new Json("Carteira adiconada com sucesso!","success");
					response.getWriter().print(new Gson().toJson(json));
					session.setCarteira(carteira);
				}else {
					//resposta em json
					response.setContentType("application/json; charset=UTF-8"); 
					Json json = new Json("Erro no servidor!","error");
					response.getWriter().print(new Gson().toJson(json));
				}
		}else {
			//resposta em json
			Json json = new Json("coloque um valor maior que a carteira atual!","error");
			response.getWriter().print(new Gson().toJson(json));
		}
    	
	}

}
