package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import controle.ContaBancariaControle;
import modelo.ContaBancaria;
import modelo.Json;
import modelo.Usuario;

/**
 * Servlet implementation class AdicionarConta
 */
@WebServlet("/AdicionarConta")
public class AdicionarConta extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("usuario") == null) {
			response.sendRedirect(request.getContextPath() + "/");
		} else {
			response.sendRedirect("dashboard.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ContaBancariaControle conC = new ContaBancariaControle();
		ContaBancaria con = new ContaBancaria();
		Usuario session = (Usuario) request.getSession().getAttribute("usuario");
		con.setConta(Integer.parseInt(request.getParameter("conta")));
		con.setValor(Double.parseDouble(request.getParameter("valor")));
		con.setTipoConta(request.getParameter("tipoConta"));
		int idUser = session.getId();
		if (conC.consultarContaBancaria(con.getConta()) == null) {
			if (conC.adicionarContaBancaria(con, idUser, Integer.parseInt(request.getParameter("agencia")))) {
				response.setContentType("application/json; charset=UTF-8");
				Json json = new Json("Conta adicionada com sucesso!", "success");
				response.getWriter().print(new Gson().toJson(json));
			} else {
				response.setContentType("application/json; charset=UTF-8");
				Json json = new Json("ERRO NO SERVIDOR!", "error");
				response.getWriter().print(new Gson().toJson(json));
			}
		}else {
			response.setContentType("application/json; charset=UTF-8");
			Json json = new Json("Número da conta já cadastrado!", "error");
			response.getWriter().print(new Gson().toJson(json));
		}
	}

}
