package servlet;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import controle.ContaBancariaControle;
import modelo.ContaBancaria;
import modelo.Usuario;

@WebServlet("/contaAjax")
public class ContaAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public ContaAjax() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Usuario user = (Usuario)request.getSession().getAttribute("usuario");
		ArrayList<ContaBancaria> contaList = new ContaBancariaControle().consultarContasBancarias(user.getId());
		response.setContentType("text/html");
		switch(request.getParameter("acao")) {
			case "contagem":
				response.getWriter().print(contaList.size());
				break;
			case "mostrar":
				int cont = 0;
				List<String> detalhes = new ArrayList<String>();
				if(contaList.size() > 0) {
					for(ContaBancaria cL : contaList) {
						String agencia = cL.getAgencia().getNome();
						String tpConta = (cL.getTipoConta().equals("corrente")) ? "Corrente" : "Poupança";
						String logo = "";
						switch(agencia) {
						 		case "Banco do Brasil": 
						  				logo = "bb-logo.png"; 
						  				break; 
						  		case "Santander":
						  			logo = "santander-logo.png"; 
						  			break; 
						  		case "Caixa Economica Federal": 
						  			logo = "caixa-logo.png"; 
						  			agencia = "Caixa Economica Federal"; 
						  			break; 
						  		case "Itau":
						  			logo = "itau-logo.png"; 
						  			agencia = "Itaú"; 
						  			break; 
						  		case "Bradesco": 
						  			logo = "bradesco-logo.png"; 
						  			agencia = "Bradesco"; 
						  			break; 
						  		default: 
						  			logo = "bb-logo.png"; 
						  	}
						Locale localeBR = new Locale( "pt", "BR" );  
						NumberFormat dinheiroBR = NumberFormat.getCurrencyInstance(localeBR);  
						String valor = dinheiroBR.format(cL.getValor());
						response.getWriter().print(" <div class='uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl'> <div class='uk-card uk-card-default uk-card-small uk-card-hover'> \n" + 
										"			 		<div class='uk-card-header'> \n" + 
										"							<div class='uk-grid uk-grid-small'> " + 
										"								<div class='uk-width-auto'>" +
										"									<a onclick='verConta(\""+cont+"\")'><h4><img width='30px' height='30px' src='imgs/"+logo+"'> - "+ agencia +"</h4></a>" +
										"								</div> "+
										"						</div> "+
										"					</div>"+
										"					<div class='uk-card-body'> "+
										"						Número da conta: <span>"+cL.getConta()+"<span><br> " +
										"						Valor disponi­vel: "+valor+"<br> " +
										"						Tipo de conta: "+ tpConta +"</div> <center>" +
										"						<button class='uk-button uk-button-default' onclick='deletarConta("+cL.getConta()+")'>DELETAR</button></center><br> " +
										"					</div> " +
										"			</div>");
							String deta = ""+
								"<h2><img width='30px' height='30px' src='imgs/"+logo+"'> - "+agencia+"</h2>"+
								"<hr></hr>"+
								"<p>Número da Conta: "+cL.getConta()+"</p>"+
								"<p>Valor disponível: "+valor+"</p>"+
								"<p>Tipo de Conta: "+tpConta+"</p>";
							
							detalhes.add(deta);
							
							cont+=1;
						}
						String detalhesString = String.join("|", detalhes);
						response.getWriter().print("<script>var detalhesString = \""+detalhesString+"\";var detalhes = detalhesString.split(\"|\");function verConta(ind){var contaDetalhes = detalhes[ind];alertify.myAlert(contaDetalhes).resizeTo('40%','40%').setHeader('');}</script>");
				}else {
					response.getWriter().print("<h2>Você não possui contas bancárias aqui.</h2>");
				}
				break;
				
			case "selectContas":
				String contas = "";
				for(int i=0;i<contaList.size();i++){
					int nConta = contaList.get(i).getConta();
					String agenciaConta = contaList.get(i).getAgencia().getNome(); 
					contas = contas + "<option value="+nConta+">"+agenciaConta+" - "+nConta+"</option>";
				}
				response.getWriter().print(contas);
				break;
			case "selectContasCartao":
				String contasCartao = "";
				for(int i=0;i<contaList.size();i++){
					int nConta = contaList.get(i).getConta();
					String agenciaConta = contaList.get(i).getAgencia().getNome(); 
					if(contaList.get(i).getTipoConta().equals("corrente")){
						contasCartao = contasCartao + "<option value='"+nConta+"'>"+agenciaConta+" - "+nConta+"</option>";
					}
				}
				response.getWriter().print(contasCartao);
				break;
		}
	}
}
