package servlet;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import javax.servlet.http.Cookie;

import controle.UsuarioControle;
import controle.CartaoControle;
import controle.ContaBancariaControle;
import controle.RegistroControle;
import modelo.Registro;

import modelo.Usuario;
import java.util.Date;

@WebServlet("/AdicionarRegistro")
public class AdicionarRegistro extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("usuario")==null) {
			response.sendRedirect(request.getContextPath() + "/");
		}else {
			response.sendRedirect("dashboard.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Date dAtual = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dAtual);
		Usuario us = (Usuario)request.getSession().getAttribute("usuario");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		String dataAtual = sdf.format(cal.getTime());
		
		String titulo=request.getParameter("titulo");
		String valorString = request.getParameter("valor");
		float valor = Float.parseFloat(valorString);
		String desc=request.getParameter("descricao");
		String repeticao = null;
		String dataOperacao = null;
		
		if(request.getParameter("dataO").equals("hoje")) {
			dataOperacao = sdf2.format(cal.getTime());
		}else {
			dataOperacao = request.getParameter("dataOperacao");
		}
		int id_Forma = 0;
		String tipoCartao = null;
		String tipoRegistro = request.getParameter("tipoRegistros");
		String dataRegistro = dataAtual;
		String forma = request.getParameter("forma");
		String fixa = request.getParameter("fixo");
		int parcelamento = 0;
		if(!(request.getParameter("parcelamento").equals("novo")) && !(request.getParameter("parcelamento").equals(""))) {
			response.getWriter().print("<script>alert("+request.getParameter("parcelamento")+")</script>");
			parcelamento = Integer.parseInt(request.getParameter("parcelamento"));
		}

		if(tipoRegistro.equals("saida")){
			if(forma.equals("carteira")) {
				repeticao = request.getParameter("repeticaoCarteira");
			}else {
				repeticao = request.getParameter("repeticao");
			}
		}else {
			if(forma.equals("carteira")) {
				repeticao = request.getParameter("repeticaoCarteira");
			}else {
				repeticao = request.getParameter("repeticao");
			}
		}
		
		Calendar dataVencimento = Calendar.getInstance();
		String dtVencimento = "2012-12-21";
		try {
			if(repeticao.equals("parcelamento")) {
				if(request.getParameter("parcelamento").equals("novo")){
					int parcelas = Integer.parseInt(request.getParameter("dataVencimento"));
					Date dtV = sdf2.parse(dataOperacao);
					dataVencimento.setTime(dtV);
					dataVencimento.add(Calendar.MONTH, parcelas);
					dtVencimento = sdf2.format(dataVencimento.getTime());
				}else{
					RegistroControle regC = new RegistroControle();
					Registro reg = regC.consultarRegistrosById(parcelamento);
					Date dtV = sdf2.parse(reg.getDataVencimento());
					dataVencimento.setTime(dtV);
					dtVencimento = sdf2.format(dataVencimento.getTime());
				}
			}else if(repeticao.equals("fixa")){
				Date dtV = sdf2.parse(dataOperacao);
				dataVencimento.setTime(dtV);
				dataVencimento.add(Calendar.MONTH, 1);
				dtVencimento = sdf2.format(dataVencimento.getTime());
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		if(forma.equals("conta")) {
			id_Forma=Integer.parseInt(request.getParameter("conta"));
		}else if(forma.equals("cartao")) {
			id_Forma=Integer.parseInt(request.getParameter("cartao"));
			tipoCartao=request.getParameter("tipoCartao");

		}
		
		
		RegistroControle regC = new RegistroControle();
		Registro reg = new Registro();
		reg.setTitulo(titulo);
		reg.setValor(valor);
		reg.setDescricao(desc);
		reg.setRepeticao(repeticao);
		reg.setTipoCartao(tipoCartao);
		reg.setTipoPagamento(forma);
		reg.setTipoRegistro(tipoRegistro);
		reg.setDataRegistro(dataAtual);
		reg.setDataOperacao(dataOperacao);
		reg.setDataVencimento(dtVencimento);
		reg.getUsuario().setId(us.getId());


		String resultado = "0";
		if(regC.adicionarRegistro(reg,us,forma,id_Forma)){
			double novoValor = valor;
			if(tipoRegistro.equals("saida")){
				novoValor = valor*(-1);
			}
			if(forma.equals("carteira")){
				UsuarioControle uC = new UsuarioControle();
				uC.atualizarCarteira(novoValor,us.getId());
				us.setCarteira(us.getCarteira() + novoValor);
			}else if(forma.equals("conta")){
				ContaBancariaControle contaC = new ContaBancariaControle();
				contaC.atualizarValor(id_Forma,novoValor);
			}else{
				CartaoControle cartaoC = new CartaoControle();
				if(tipoCartao.equals("credito")){
					cartaoC.atualizarValor(id_Forma,novoValor);
				}else{
					CartaoControle cartC = new CartaoControle();
					int cart = cartC.consultarCartoesById(id_Forma).getConta().getConta();
					if(cart!=0){
						ContaBancariaControle contaC = new ContaBancariaControle();
						contaC.atualizarValor(cart,novoValor);	
					}else{
						cartaoC.atualizarValor(id_Forma,novoValor);
					}
					
				}
				
			}

			if(repeticao.equals("fixa") && !(fixa.equals("novo"))){
				regC.atualizarVencimento(Integer.parseInt(request.getParameter("fixo")));

			}
			if(repeticao.equals("parcelamento") && !(request.getParameter("parcelamento").equals("novo"))){
				regC.atualizarVencimento(parcelamento);
			}
			resultado = "1";
		}
		
		
		Cookie cookie = new Cookie("addRegistroResult",resultado);
		response.addCookie(cookie);
		response.sendRedirect("dashboard.jsp");
	}

}
