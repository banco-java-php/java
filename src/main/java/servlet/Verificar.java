
package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controle.UsuarioControle;
import modelo.Usuario;

/**
 * Servlet implementation class Verificar
 */
@WebServlet("/Verificar")
public class Verificar extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("usuario")==null) {
			response.sendRedirect(request.getContextPath() + "/");
		}else {
			response.sendRedirect("dashboard.jsp");
		}
	}
    public String criptografar(char carac) {
    	String senha = ""+carac+"";
    	switch(senha) {
    	case "a":
    		senha = "b$";
    		break;
    	case "b":
    		senha = "cK*L";
    		break;
    	case "c":
    		senha = "dgd";
    		break;
    	case "d":
    		senha = "e452#";
    		break;
    	case "e":
    		senha = "f@35";
    		break;
    	case "f":
    		senha = "g$5g";
    		break;
    	case "g":
    		senha = "h@30";
    		break;
    	case "h":
    		senha = "i25";
    		break;
    	case "i":
    		senha = "jçlujj";
    		break;
    	case "j":
    		senha = "k!@99";
    		break;
    	case "k":
    		senha = "l&**4";
    		break;
    	case "l":
    		senha = "m/f3";
    		break;
    	case "m":
    		senha = "njjhk";
    		break;
    	case "n":
    		senha = "o?343@";
    		break;
    	case "o":
    		senha = "p@#5";
    		break;
    	case "p":
    		senha = "q!@37";
    		break;
    	case "q":
    		senha = "r#45";
    		break;
    	case "r":
    		senha = "sRytut";
    		break;
    	case "s":
    		senha = "tggs";
    		break;
    	case "t":
    		senha = "uaa77";
    		break;
    	case "u":
    		senha = "v|xf";
    		break;
    	case "v":
    		senha = "w!32rs";
    		break;
    	case "w":
    		senha = "xrrg";
    		break;
    	case "x":
    		senha = "ywy&";
    		break;
    	case "y":
    		senha = "zerw6#";
    		break;
    	case "z":
    		senha = "a!!#&";
    		break;
    	case "0":
    		senha = ""+(Integer.parseInt(senha)*3)+"";
    		break;
    	case "1":
    		senha = ""+(Integer.parseInt(senha)*4)+"";
    		break;
    	case "2":
    		senha = ""+(Integer.parseInt(senha)*8)+"";
    		break;
    	case "3":
    		senha = ""+(Integer.parseInt(senha)*7)+"";
    		break;
    	case "4":
    		senha = ""+(Integer.parseInt(senha)*6)+"";
    		break;
    	case "5":
    		senha = ""+(Integer.parseInt(senha)*2)+"";
    		break;
    	case "6":
    		senha = ""+(Integer.parseInt(senha)*6)+"";
    		break;
    	case "7":
    		senha = ""+(Integer.parseInt(senha)*9)+"";
    		break;
    	case "8":
    		senha = ""+(Integer.parseInt(senha)*5)+"";
    		break;
    	case "9":
    		senha = ""+(Integer.parseInt(senha)*2)+"";
    		break;
    	}
    	return senha;
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("usuario")==null) {
			try {
				UsuarioControle controle= new UsuarioControle();
				Usuario user= new Usuario();
				String email = request.getParameter("email");
				user.setEmail(email);
				String senha = request.getParameter("senha");
				List<String> senhaNova = new ArrayList<String>();
				for(int i=0;i<senha.length();i++) {
					senhaNova.add(criptografar(senha.charAt(i)));
				}
				String senhaNova2 = String.join("",senhaNova);
				user.setSenha(senhaNova2);
				Usuario us = controle.verificar(user);
				if(us!=null) {
					session.setAttribute("usuario", us);
					response.sendRedirect("dashboard.jsp");
				}else {
					response.sendRedirect("login.jsp");
				}
				
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}
		}else {
			response.sendRedirect("dashboard.jsp");
		}
	}

}
