package servlet;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controle.UsuarioControle;

@WebServlet(asyncSupported = true, urlPatterns = { "/email" })
public class VerificarEmail extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		//validar email
		Matcher matcher = Pattern.compile("^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,6}$", Pattern.CASE_INSENSITIVE).matcher(email);
		if(!matcher.find()){
			response.setContentType("text/plain;charset=UTF-8");
			response.getWriter().print("Email inválido.");
		}else if(new UsuarioControle().verificarEmail(email)){
			response.setContentType("text/plain;charset=UTF-8");
			response.getWriter().print("Email já cadastrado.");
		}else if(email.length() >= 60){
			response.setContentType("text/plain;charset=UTF-8");
			response.getWriter().print("Email maior que 60 caracteres.");
		}else {
		}
			response.setContentType("text/plain;charset=UTF-8");
			response.getWriter().print("");
		}
	}

