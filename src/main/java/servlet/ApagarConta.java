package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import controle.ContaBancariaControle;
import modelo.Json;

@WebServlet("/apagarConta")
public class ApagarConta extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public ApagarConta() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ContaBancariaControle conC = new ContaBancariaControle();
		if(conC.deletar(Integer.parseInt(request.getParameter("id")))) {
			response.setContentType("application/json; charset=UTF-8");
			Json json = new Json("Conta deletada com sucesso!", "success");
			response.getWriter().print(new Gson().toJson(json));
		}else {
			response.setContentType("application/json; charset=UTF-8");
			Json json = new Json("Erro ao apagar a conta!", "error");
			response.getWriter().print(new Gson().toJson(json));
		}
	}

}
