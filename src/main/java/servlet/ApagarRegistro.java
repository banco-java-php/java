package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import controle.RegistroControle;
import modelo.Json;
import modelo.Usuario;

@WebServlet("/apagarRegistro")
public class ApagarRegistro extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public ApagarRegistro() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario us = (Usuario)request.getSession().getAttribute("usuario");
		RegistroControle regC = new RegistroControle();
		if(regC.deletar(Integer.parseInt(request.getParameter("id")),request.getParameter("tipoPagamento"),Double.parseDouble(request.getParameter("valor")),Integer.parseInt(request.getParameter("id_forma")))) {
			if(request.getParameter("tipoPagamento").equals("carteira")) {
				us.setCarteira(us.getCarteira() + Double.parseDouble(request.getParameter("valor"))*(-1));
			}
			
			response.setContentType("application/json; charset=UTF-8");
			Json json = new Json("Registro removido com sucesso!", "success");
			response.getWriter().print(new Gson().toJson(json));
			
		}else {
			response.setContentType("application/json; charset=UTF-8");
			Json json = new Json("Erro ao remover o registro!", "error");
			response.getWriter().print(new Gson().toJson(json));
		}
	}

}
