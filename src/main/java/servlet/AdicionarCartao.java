package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import controle.CartaoControle;
import modelo.Cartao;
import modelo.Json;
import modelo.Usuario;

@WebServlet("/AdicionarCartao")
public class AdicionarCartao extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("usuario")==null) {
			response.sendRedirect(request.getContextPath() + "/");
		}else {
			response.sendRedirect("dashboard.jsp");
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CartaoControle carC = new CartaoControle();
		Cartao car = new Cartao();
		int conta = Integer.parseInt(request.getParameter("conta"));
		int numero = Integer.parseInt(request.getParameter("numero"));
		double limite = Double.parseDouble(request.getParameter("limite"));
		Usuario session = (Usuario)request.getSession().getAttribute("usuario");
		boolean debito = Boolean.parseBoolean(request.getParameter("debito"));
		car.setId(numero);
		if(debito) {
			car.setLimite(0);
			car.setValor(0);
		}else {
			car.setLimite(limite);
			car.setValor(limite);
		}
		
		car.getUsuario().setId(session.getId());
		if(conta != 0) {
			car.getConta().setConta(conta);
		}
		Cartao veri = carC.consultarCartoesById(car.getId());
		if ( veri == null) {
			if (carC.adicionarCartao(car)) {
				response.setContentType("application/json; charset=UTF-8");
				Json json = new Json("Cartão adicionado com sucesso!", "success");
				response.getWriter().print(new Gson().toJson(json));
			} else {
				response.setContentType("application/json; charset=UTF-8");
				Json json = new Json("ERRO NO SERVIDOR!", "error");
				response.getWriter().print(new Gson().toJson(json));
			}
		}else {
			response.setContentType("application/json; charset=UTF-8");
			Json json = new Json("Número do cartão já cadastrado!", "error");
			response.getWriter().print(new Gson().toJson(json));
		}
	}

}
