package servlet;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controle.CartaoControle;
import modelo.Cartao;
import modelo.Usuario;

@WebServlet("/cartaoAjax")
public class CartaoAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public CartaoAjax() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Usuario user = (Usuario)request.getSession().getAttribute("usuario");
			ArrayList<Cartao> cartaoList= new CartaoControle().consultarCartoes(user.getId()); 
			response.setContentType("text/html");
		switch(request.getParameter("acao")) {
			case "contagem":
				response.getWriter().print(cartaoList.size());
				break;
			case "mostrar":
				int cont = 0;
				List<String> detalhes = new ArrayList<String>();
				if(cartaoList.size() > 0) {
					for(Cartao cL : cartaoList) {
						String nCartao = Integer.toString(cL.getId()); 
						String nCartaoUltimo = nCartao.substring(5);
						String nCartaoEscondido = "**** **** **** "+nCartaoUltimo;
						String agencia = (cL.getConta().getAgencia().getNome() != null ) ? cL.getConta().getAgencia().getNome() : "";
						String logo = "";
						switch (agencia) {
							case "Banco do Brasil":
								logo = "bb-logo.png";
								agencia = "Cartão Banco do Brasil";
								break;
							case "Santander":
								logo = "santander-logo.png";
								agencia = "Cartão Santander";
								break;
							case "Caixa Economica Federal":
								logo = "caixa-logo.png";
								agencia = "Cartão Caixa Econômica Federal";
								break;
							case "Itau":
								logo = "itau-logo.png";
								agencia = "Cartão Itaú";
								break;
							case "Bradesco":
								logo = "bradesco-logo.png";
								agencia = "Cartão Bradesco";
								break;
							default:
								logo = "";
								agencia = "Cartão sem Banco";
								break;
						}					
					String logoDetalhes = (logo!="") ?"<img style='width:120px;height:120px;' src='imgs/"+logo+"'>" :"";
					logo = (logo!="") ?"<img style='width:60px;height:60px;' src='imgs/"+logo+"'>" :"";
					Locale localeBR = new Locale( "pt", "BR" );  
					NumberFormat dinheiroBR = NumberFormat.getCurrencyInstance(localeBR);  
					String limite =(cL.getLimite()!=0) ?"<p>Limite: "+dinheiroBR.format(cL.getLimite())+"</p>" :"";  
					String valor = (cL.getLimite()!=0) ?"<p>"+dinheiroBR.format(cL.getValor())+"</p>" :"<p>Valor disponível no banco: " + dinheiroBR.format(cL.getConta().getValor())+"</p>";
					response.getWriter().print(
					"<div class='uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl'>"+
						"<div class='uk-card uk-card-default uk-card-small uk-card-hover'>"+
						"	<div class='uk-card-header'>"+
							"	<div class='uk-grid uk-grid-small'>"+
							"		<div class='uk-width-auto'><h4>"+agencia+"</h4></div>"+
							"	</div>"+
							"</div>"+
							"<div class='uk-card-body'>"+
						"		<a onclick='verCartao("+cont+")'><div style='position:relative;'><img style='width:100%;height:100%;' src='imgs/cartao.png'><div style='width:80%;heigth:100%;position:absolute;color:white;font-size:24px;word-spacing:11px;top:57%;left:50%;transform:translate(-50%,-50%);'>"+nCartaoEscondido+"</div><div style='width:80%;heigth:100%;position:absolute;color:white;font-size:24px;word-spacing:11px;top:82%;left:50%;transform:translate(-50%,-50%);'>"+user.getNome()+"</div><div style='width:80%;heigth:100%;position:absolute;top:65%;left:65%;transform:translate(10%,0%);'>"+logo+"</div></div></a>"+
						"	</div>"+
						"	<center><button class='uk-button uk-button-default' onclick='deletarCartao("+cL.getId()+")'>Deletar Cartão</button></center><br>"+
					"	</div>"+
					"</div>"
					);
	
					String deta = ""+
					"<h2>"+agencia+"</h2>"+
					"<hr></hr>"+
					"<div class='uk-grid-small' uk-grid>"+
					"<div class='uk-width-1-2@s'>"+
					"<p>Número Cartão: "+nCartaoEscondido+"</p>"+
					limite+
					valor+ 
					"</div>"+
					"<div class='uk-width-1-2@s'>"+logoDetalhes+
					"</div></div>";
					cont+=1;
					detalhes.add(deta);
				}
				String detalhesString = String.join("|", detalhes);
				response.getWriter().print("<script>var detalhesString = \""+detalhesString+"\";var detalhes = detalhesString.split(\"|\");function verCartao(ind){var cartaoDetalhes = detalhes[ind];alertify.myAlert(cartaoDetalhes).resizeTo('40%','40%').setHeader('');}</script>");
			}else {
				response.getWriter().print("<h2>Você não possui cartão aqui.</h2>");
			}
				break;
			case "selectCartao":
				String cartoes = "";
				for(int i=0;i<cartaoList.size();i++){
					String nCartao = Integer.toString(cartaoList.get(i).getId()).substring(0, 4);
					String agencia = "";
					if(cartaoList.get(i).getConta().getConta() != 0){
						agencia = cartaoList.get(i).getConta().getAgencia().getNome();
					}
					String apenasDebito = "";
					
					double carLimite = cartaoList.get(i).getLimite();
					int carConta = cartaoList.get(i).getConta().getConta();
					if(carLimite==0 || carConta==0){
						apenasDebito = "class='opcoesCredito'";
					}
					
					nCartao = nCartao + " **** **** ****";
					if(agencia.equals("")) {
						cartoes = cartoes + "<option "+apenasDebito+" value="+cartaoList.get(i).getId()+"> Sem conta bancaria - "+nCartao+"</option>";						
					}else {
						cartoes = cartoes + "<option "+apenasDebito+" value="+cartaoList.get(i).getId()+">"+agencia+" - "+nCartao+"</option>";
					}
				}
				response.getWriter().print(cartoes);
		}
	}
}
