package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;
import java.util.ArrayList;
import java.util.Date;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import modelo.Usuario;
import modelo.Registro;
import java.lang.Exception;

import controle.RegistroControle;

@WebServlet("/filtragem")
public class Filtragem extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Usuario user = (Usuario) request.getSession().getAttribute("usuario");
		List<String> detalhes = new ArrayList<String>();
		String detalheString = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM");
		SimpleDateFormat sdf4 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date data = new Date();
		if (user != null) {
			String filtro = request.getParameter("filtro");
			String valorr = request.getParameter("valor");
			ArrayList<Registro> regC = new RegistroControle().consultarRegistrosFiltro(user.getId(), filtro, valorr);
			if (regC.size() > 0) {
				int cont = 0;
				try {
					response.setContentType("text/html;charse=UTF-8");
					for (Registro rL : regC) {
						String titulo = rL.getTitulo();
						double valor = rL.getValor();
						Locale localeBR = new Locale("pt", "BR");
						NumberFormat dinheiroBR = NumberFormat.getCurrencyInstance(localeBR);
						String valorForm = dinheiroBR.format(rL.getValor());
						String tpRegistro = "plus.png";
						String tipoRegistro = "Entrada";
						if (rL.getTipoRegistro().equals("saida")) {
							valor = valor * (-1);
							tpRegistro = "less.png";
							tipoRegistro = "Saída";
						}
						double valorNormal = valor;

						int idForma = user.getId();
						String meioDetalhado = "";
						String meio = "";
						if (rL.getTipoPagamento().equals("carteira")) {
							meio = "Carteira";
							meioDetalhado = "Carteira";
						} else if (rL.getTipoPagamento().equals("conta")) {
							meio = "Conta Bancaria";
							meioDetalhado = "Conta Bancaria " + rL.getConta().getConta() + " - "
									+ rL.getConta().getAgencia().getNome();
							idForma = rL.getConta().getConta();
						} else {
							meio = "Cartão";
							if (rL.getCartao().getConta().getConta() == 0) {
								meioDetalhado = "Cartão " + rL.getCartao().getId() + " - "
										+ rL.getCartao().getConta().getAgencia().getNome();
							} else {
								meioDetalhado = "Cartão " + rL.getCartao().getId() + " - Sem Banco";
							}

							idForma = rL.getCartao().getId();
						}

						Date dataReg = sdf1.parse(rL.getDataRegistro());
						Date dataOp = sdf.parse(rL.getDataOperacao());
						String dataVen = "";

						if (rL.getDataVencimento() != null) {
							Date dtVen = sdf.parse(rL.getDataVencimento());
							Calendar dataVenCal = Calendar.getInstance();
							dataVenCal.setTime(dtVen);
							dataVen = "Dia de Vencimento: " + sdf3.format(dtVen) + " <> Até " + sdf2.format(dtVen);
						}
						Calendar dataRegCal = Calendar.getInstance();
						dataRegCal.setTime(dataReg);
						Calendar dataOpCal = Calendar.getInstance();
						dataOpCal.setTime(dataOp);
						response.getWriter().print("<tr>");
						response.getWriter()
								.print("<td><img style='width:15px;height:15px' src='imgs/" + tpRegistro + "'></td>");
						response.getWriter().print("<td>" + titulo + "</td>");
						response.getWriter().print("<td>" + valorForm + "</td>");
						response.getWriter().print("<td>" + meio + "</td>");
						response.getWriter().print("<td>" + sdf4.format(dataReg) + "</td>");
						response.getWriter().print("<td><a class='uk-icon-button verRegistro' onclick='verRegistro("
								+ cont + ")'><span uk-icon='icon: search'></span></a>");
						response.getWriter()
								.print("<a onclick='deletarRegistro(" + rL.getId() + "," + rL.getTipoPagamento() + ","
										+ valorNormal + "," + idForma
										+ ")' class='uk-icon-button'><span uk-icon='icon: close'></span></a></td>");
						response.getWriter().print("</tr>");

						String deta = "" + "<h2>" + titulo + " <a class='uk-icon-button btnPDF2' id='" + cont
								+ "'><span uk-icon='icon: download'></span></a></h2>" + "<hr></hr>" + "<p>Valor: "
								+ valorForm + "</p>" + "<p>Tipo de registro: " + tipoRegistro + "</p>" + "<p>Meio: "
								+ meioDetalhado + "</p>" + "<p>Data de registro: " + sdf4.format(dataReg) + "</p>"
								+ "<p>Data de operação: " + sdf2.format(dataOp) + "</p>" + "<p>" + dataVen + "</p>"
								+ "<hr class='uk-divider-icon'></hr>" + "<p>" + rL.getDescricao() + "</p>";

						detalhes.add(deta);
						detalheString = String.join("|", detalhes);
						cont += 1;
					}
					
					response.getWriter().print(  
							"<script type='text/javascript'>\n" + 
							"var detalhesString = \""+ detalheString+"\""+ 
							";var detalhes = detalhesString.split(\"|\");"+
							"			function savePDF2(codigoHTML) {\n" + 
							"			  var doc = new jsPDF('portrait', 'pt', 'a4');\n" + 
							"			   var  data = new Date();\n" + 
							"			  margins = {\n" + 
							"			    top: 40,\n" + 
							"			    bottom: 60,\n" + 
							"			    left: 40,\n" + 
							"			    width: 1000\n" + 
							"			  };\n" + 
							"			  doc.fromHTML(codigoHTML,\n" + 
							"			               margins.left, // x coord\n" + 
							"			               margins.top, { pagesplit: true },\n" + 
							"			               function(dispose){\n" + 
							"			    doc.save(\"registro - \"+data.getDate()+\"/\"+(data.getMonth()+1)+\"/\"+data.getFullYear()+\".pdf\");\n" + 
							"			  });\n" + 
							"			}\n" + 
							"			$('.verRegistro').on('click',function(){\n" + 
							"		        $(\".btnPDF2\").click(function(event) {\n" + 
							"		        	var det = detalhes[$(this).attr('id')];\n" + 
							"		        	var detalhesPDF = \"<img src='imgs/money.png' width='40px'><h1>yourMoney</h1><hr>\"+det;\n" + 
							"		            savePDF2(detalhesPDF);\n" + 
							"		            alertify.closeAll();\n" + 
							"		        });\n" + 
							"		    });\n" + 
							"\n" + 
							"		    \n"+ 
							"</script>\n");
					/**function savePDF(codigoHTML) {
						  var doc = new jsPDF('portrait', 'pt', 'a4'),
						      data = new Date();
						  margins = {
						    top: 40,
						    bottom: 60,
						    left: 40,
						    width: 1000
						  };
						  doc.fromHTML(codigoHTML,
						               margins.left, // x coord
						               margins.top, { pagesplit: true },
						               function(dispose){
						    doc.save(\"registro - \"+data.getDate()+\"/\"+(data.getMonth()+1)+\"/\"+data.getFullYear()+\".pdf");
						  });
						}
						$('.verRegistro').on('click',function(){
					        $(".btnPDF").click(function(event) {
					        	var det = detalhes[$(this).attr('id')];
					        	var detalhesPDF = \"<img src='imgs/money.png' width='40px'><h1>yourMoney</h1><hr>\"+det;
					            savePDF(detalhesPDF);
					            alertify.closeAll();
					        });
					    });*/

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			} else {
				response.getWriter().print("Sem resultados.");
			}
		}

	}
}
