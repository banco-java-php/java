package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import controle.CartaoControle;
import modelo.Json;

@WebServlet("/apagarCartao")
public class ApagarCartao extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public ApagarCartao() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CartaoControle conC = new CartaoControle();
		if(conC.deletar(Integer.parseInt(request.getParameter("id")))) {
			response.setContentType("application/json; charset=UTF-8");
			Json json = new Json("Cartao removido com sucesso!", "success");
			response.getWriter().print(new Gson().toJson(json));
		}else {
			response.setContentType("application/json; charset=UTF-8");
			Json json = new Json("Erro ao remover cartao!", "error");
			response.getWriter().print(new Gson().toJson(json));
		}
	}

}
