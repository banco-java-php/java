package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Usuario;

@WebServlet("/carteiraAjax")
public class CarteiraAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public CarteiraAjax() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario user = (Usuario)request.getSession().getAttribute("usuario");
		if(user != null) {
			
			response.getWriter().print("<script>");
			response.getWriter().print("var valor = ("+user.getCarteira()+").toLocaleString('pt-BR', {\n" + 
					"				style : 'currency',\n" + 
					"				currency : \"BRL\"\n" + 
					"			});"
					+ "$('#carteiraDash').html(valor)");
			response.getWriter().print("</script>");
		}
	}

}
