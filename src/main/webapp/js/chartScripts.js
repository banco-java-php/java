// Chart 1
// ========================================================================
//retorna o numero formatado para valor monetario
function format(value){
	value = parseFloat(value);
	return value.toLocaleString("pt-BR",{style:"currency", currency:"BRL"})
}
// Chart 2
// ========================================================================
var char2El = document.getElementById('chart2');

new Chart(char2El, {
  type: 'line',
  
  data: {
    labels: labels,
    datasets: [{
      data: carteiraData,
      label: "Carteira",
      borderColor: "#39f",
      fill: true
    }]
  },
  options: {
    maintainAspectRatio: false,
    responsiveAnimationDuration: 500,
    animation: {
      duration: 5000
    },
    tooltips:{
    	callbacks: {
    		label: (tooltipItem, data) => {
    			var valor = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
    			return format(valor);
    		},
    	},
    },
    title: {
      display: true,
      text: 'Sua carteira'
    },
    scales: {
       yAxes: [{
         ticks: {
           callback: function(value, index, values) {
             return value.toLocaleString("pt-BR",{style:"currency", currency:"BRL"});
           }
         }
       }]
    }

  }
});


// Chart 3
// ========================================================================
var char3El = document.getElementById('chart3');

new Chart(char3El, {
  type: 'bar',
  data: {
    labels: ["Carteira", "Contas Bancárias", "Cartões de Crédito"],
    datasets: [{
      label: "Saída mensal",
      backgroundColor: ["#39f", "#895df6", "#3cba9f", "#e8c3b9", "#c45850"],
      data: [saidaTotalData0,saidaTotalData1,saidaTotalData2]
    }]
  },
  options: {
    maintainAspectRatio: false,
    responsiveAnimationDuration: 500,
    legend: {
      display: false
    },
    animation: {
      duration: 5000
    },
    tooltips:{
    	callbacks: {
    		label: (tooltipItem, data) => {
    			var valor = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
    			return format(valor);
    		},
    	},
    },
    title: {
      display: true,
      text: 'Saída mensal'
    },
    scales: {
       yAxes: [{
         ticks: {
           callback: function(value, index, values) {
             return value.toLocaleString("pt-BR",{style:"currency", currency:"BRL"});
           }
         }
       }]
    }
  }
});

// Chart 4
// ========================================================================
var char4El = document.getElementById('chart4');

new Chart(char4El, {
  type: 'bar',
  data: {
    labels: ["Carteira", "Contas Bancárias", "Cartões de Crédito"],
    datasets: [{
      label: "Entrada mensal",
      backgroundColor: ["#39f", "#895df6", "#3cba9f", "#e8c3b9", "#c45850"],
      data: [entradaTotalData0,entradaTotalData1,entradaTotalData2]
    }]
  },
  options: {
    maintainAspectRatio: false,
    responsiveAnimationDuration: 500,
    legend: {
      display: false
    },
    animation: {
      duration: 5000
    },
    tooltips:{
    	callbacks: {
    		label: (tooltipItem, data) => {
    			var valor = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
    			return format(valor);
    		},
    	},
    },
    title: {
      display: true,
      text: 'Entrada mensal'
    },
    scales: {
       yAxes: [{
         ticks: {
           callback: function(value, index, values) {
             return value.toLocaleString("pt-BR",{style:"currency", currency:"BRL"});
           }
         }
       }]
    }
  }
});

var char5El = document.getElementById('chart5');
  var totalEntrada=parseFloat(entradaTotalData0)+parseFloat(entradaTotalData1)+parseFloat(entradaTotalData2);
  var totalSaida=parseFloat(saidaTotalData0)+parseFloat(saidaTotalData1)+parseFloat(saidaTotalData2);
  var valorSaida=totalSaida*(-1);
  if(totalEntrada>totalSaida){
      var b = totalEntrada-totalSaida;
      b = format(b);
      var a= "Positivo! A sobrar: " +b;
  }else{
      var b =totalSaida-totalEntrada;
      b = format(b);
      var a="Negativo! A dever: " +b;
  }
  new Chart(char5El,{
    type: 'doughnut',
    data: {
      labels: ["Entradas","Saídas"],
      datasets: [
        {
          label: "Balanço Geral",
          backgroundColor: ["#3e95cd", "#c00"],
          data: [totalEntrada,totalSaida]
        }
      ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      tooltips:{
      	callbacks: {
      		label: (tooltipItem, data) => {
      			var valor = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
      			return format(valor);
      		},
      	},
      },
      title: {
        display: true,
        text: 'Relação Ganho/Gasto: '+a
      }
      
    }

});