var contas = "";
$.ajax({
	url: "contaAjax?acao=selectContas",
	success: function(response){
		contas = response;
	}
});

function verificarConta() {
	var input = document.getElementById("Nconta");
	var Nconta = input.inputmask.unmaskedvalue();
	var valor = $("#valor1").maskMoney("unmasked")[0];
	var tipoConta = $("#tipoConta").val();
	var agencia = $("#agencia").val();
	var resultado = false;
	if (Nconta != "") {
		if (valor > 0) {
			if (tipoConta != "") {
				if (agencia != "") {
					resultado = true;
				} else {
					$("#avisos").fadeIn("fast", function() {
						$("#avisos").html("Selecione a agencia!");
					});
					$("#avisos").fadeOut(10000);
				}
			} else {
				$("#avisos").fadeIn("fast", function() {
					$("#avisos").html("Selecione o tipo da conta!");
				});
				$("#avisos").fadeOut(10000);
			}
		} else {
			$("#avisos").fadeIn("fast", function() {
				$("#avisos").html("Valor não pode ser vazio!");
			});
			$("#avisos").fadeOut(10000);
		}
	} else {
		$("#avisos").fadeIn("fast", function() {
			$("#avisos").html("Coloque o número da conta!");
		});
		$("#avisos").fadeOut(10000);
	}
	return resultado;
}
function adicionarConta(local) {
	maskConta();
	$("#formContas").submit(function(e) {
		e.preventDefault();
		if (verificarConta()) {
			var input = document.getElementById("Nconta");
			var Nconta = input.inputmask.unmaskedvalue();
			var valor = $("#valor1").maskMoney("unmasked")[0];
			var tipoConta = $("#tipoConta").val();
			var agencia = $("#agencia").val();
			$.ajax({
				url : "AdicionarConta",
				method : "POST",
				dataType : "JSON",
				data : {
					conta : Nconta,
					valor : valor,
					tipoConta : tipoConta,
					agencia : agencia
				},
				beforeSend: function() {
					$("#adicionarConta").html("");
					$("#adicionarConta").attr("disabled","");
					$("#adicionarConta").attr("uk-spinner","");
				},
				error : function() {
					alertify.error("Erro no servidor!");
					$("#adicionarConta").html("adicionar");
					$("#adicionarConta").removeAttr("disabled");
					$("#adicionarConta").removeAttr("uk-spinner");
				},
				success : function(resposta) {
					$("#adicionarConta").html("adicionar");
					$("#adicionarConta").removeAttr("disabled");
					$("#adicionarConta").removeAttr("uk-spinner");
					if (resposta.codigo == 1) {
						$.ajax({
							url: "contaAjax?acao=selectContas",
							success: function(response){
								contas = response;
							}
						});
						$.ajax({
							url: "contaAjax?acao=selectContasCartao",
							success: function(response){
								contasCartao = response;
							}
						});
						alertify.success(resposta.mensagem);
						document.forms[0].reset();
						if(local == "dashboard"){
							$.ajax({
								url: "contaAjax?acao=contagem",
								success: function(response){
									if(response != 0 && !$("#temConta").is(":visible")){
										$("#NtemConta").hide(1000);
										$("#temConta").show(1000, function(){
											$(".botaoConta").on("click",function(){
												alertify.myAlert(formConta).resizeTo('40%','80%').setHeader('<h2>Adicionar Conta Bancária</h2>');
												adicionarConta("dashboard");
											});
										});
										$("#numeroDeContas").html(response);
									}else{
										$("#numeroDeContas").html(response);
									}
								}
							});
						}else{
							$.ajax({
								url: "contaAjax?acao=mostrar",
								method: "GET",
								success: function (response){
									if(response != ""){
										$("#mostrarContas").html(response);
									}else{
										$("#mostrarContas").html("<h1 class='uk-text-center'>Você não tem conta registrada!</h1>");
									}	
								}
							});
						}
						
					}else{
						alertify.error(resposta.mensagem);
					}
				}

			});
		}
	})
}