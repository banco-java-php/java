function mudarForma() {
	$("#conta").html("<option value=''>Selecionar</option>"+contas);
	$("#cartao").html("<option value=''>Selecionar</option>"+cartoes);
	if($('#forma').val()=="carteira"){
		$('#fieldFix').hide();
		$('#fieldConta').hide();
		$("#conta").prop('required',false);
		$('#fieldCartao').hide();
		$("#cartao").prop('required',false);
		$('#fieldTipoCartao').hide();
		$("#procedimento").prop('required',false);
		$('#fieldRepeticao').hide();
		$("#repeticao").prop('required',false);
		if($('#tipoRegistros').val()=="saida"){
			$('#fieldRepeticaoCarteira').show();
			$("#repeticaoCarteira").prop('required',true);
		}else {
			$('#fieldRepeticaoCarteira').hide();
			$("#repeticaoCarteira").prop('required',false);
		}
		if($('#repeticaoCarteira').val()!="fixa"){
			$('#fieldFixo').hide();
			$("#fixo").prop('required',false);	
		}
		if($('#repeticaoCarteira').val()!="parcelamento"){
			$('#fieldParcelamento').hide();
			$("#parcelamento").prop('required',false);
		}
		$('#fieldRepeticao').hide();
		$("#repeticao").prop('required',false);
	}else if($('#forma').val()=="conta") {
		$('#fieldFix').show();
		$('#fieldCartao').hide();
		$("#cartao").prop('required',false);
		$('#fieldTipoCartao').hide();
		$("#tipoCartao").prop('required',false);
		if($('#tipoRegistros').val()=="saida"){
			$('#fieldRepeticao').show();
			$("#repeticao").prop('required',true);
		}else {
			$('#fieldRepeticao').hide();
			$("#repeticao").prop('required',false);

		}
		if($('#repeticao').val()!="fixa"){
			$('#fieldFixo').hide();
			$("#fixo").prop('required',false);
		}
		if($('#repeticao').val()!="parcelamento"){
			$('#fieldParcelamento').hide();	
			$("#parcelamento").prop('required',false);
		}else{
			$('#fieldParcelamento').show();	
			$("#parcelamento").prop('required',true);
		}
		$('#fieldConta').show();
		$("#conta").prop('required',true);
		$('#fieldRepeticaoCarteira').hide();
		$("#repeticaoCarteira").prop('required',false);
	}else {
		$('#fieldFix').show();
		$('#fieldConta').hide();
		$("#conta").prop('required',false);
		if($('#tipoRegistros').val()=="saida"){
			$('#fieldRepeticao').show();
			$("#repeticao").prop('required',true);
		}else {
			$('#fieldRepeticao').hide();
			$("#repeticao").prop('required',false);
		}
		if($('#repeticao').val()!="fixa"){
			$('#fieldFixo').hide();
			$("#fixo").prop('required',false);
		}
		if($('#repeticao').val()!="parcelamento"){
			$('#fieldParcelamento').hide();	
			$("#parcelamento").prop('required',false);
		}else{
			$('#fieldParcelamento').show();	
			$("#parcelamento").prop('required',true);
		}
		$('#fieldRepeticaoCarteira').hide();
		$("#repeticaoCarteira").prop('required',false);
		$('#fieldCartao').show();
		$("#cartao").prop('required',true);
		$('#fieldTipoCartao').show();
		$("#procedimento").prop('required',true);
	}
}

function mudarTipoRegistro(){
	if($('#tipoRegistros').val()=="entrada"){
		$("#formaCartao").wrap('<span/>');
		mudarForma();
	}else{
		$("#formaCartao").unwrap();
		mudarForma();
	}
}

function mudarProcedimento(){
	if($('#procedimento').val()=="credito"){
		if($('#repeticao').children('option').length==2){
			$("#repetParcelamento").unwrap();
		}
		$(".opcoesCredito").wrap('<span/>');
		mudarForma();
	}else if($('#procedimento').val()=="debito"){
		if($('#repeticao').children('option').length==3){
			$("#repetParcelamento").wrap('<span/>');
		}
		$(".opcoesCredito").unwrap();
		mudarForma();
	}
}

function mudarDataOperacao() {
	if($('#dataO').val()=="hoje"){
		$('#fieldDataOperacao').hide();
		$("#dataOperacao").prop('required',false);
	}else{
		$('#fieldDataOperacao').show();
		$("#dataOperacao").prop('required',true);
	}
	
}

function mudarRepeticao() {

	if($('#tipoRegistros').val()=="saida"){
		if($('#forma').val()=="carteira"){
			$('#fieldDataVencimento').hide();
			$("#dataVencimento").prop('required',false);
		}else{

			if($('#repeticao').val()=="parcelamento"){
				if($('#parcelamento').val()=="novo"){
					$('#fieldDataVencimento').show();
					$("#dataVencimento").prop('required',true);
				}else{
					$('#fieldDataVencimento').hide();
					$("#dataVencimento").prop('required',false);
				}

			}else{
				$('#fieldDataVencimento').hide();
				$("#dataVencimento").prop('required',false);
			}
		}
		
	}else{
		$('#fieldDataVencimento').hide();
		$("#dataVencimento").prop('required',false);
	}

}

function mudarFixo(){
	if($('#forma').val()=="carteira"){
		if($('#repeticaoCarteira').val()=="fixa"){
			$('#fieldFixo').show();
			$("#fixo").prop('required',true);
		}else{
			$('#fieldFixo').hide();
			$("#fixo").prop('required',false);	
		}
	}else{
		if($('#repeticao').val()=="fixa"){
			$('#fieldFixo').show();
			$("#fixo").prop('required',true);
		}else{
			$('#fieldFixo').hide();
			$("#fixo").prop('required',false);
		}
	}

}

function mudarParcelamento(){
	if($('#forma').val()!="carteira"){
		if($('#repeticao').val()=="parcelamento"){
			$('#fieldParcelamento').show();
			$("#parcelamento").prop('required',true);
		}else{
			$('#fieldParcelamento').hide();
			$("#parcelamento").prop('required',false);
		}
	}else{
		$('#fieldParcelamento').hide();
		$("#parcelamento").prop('required',false);
	}

}

function filtro(){
	$("#filtroForm").submit(function(e){
			e.preventDefault();
			var filtro = $("#filtro").val();
			if(filtro == "valor"){
				var valor = $("#valor").maskMoney("unmasked")[0];
			}else{
				var valor = $("#"+filtro).val();
			}
			
			$.ajax({
				url: "filtragem?filtro="+filtro+"&valor="+valor,
				method: 'GET',
				before: function(){
					$("#recebeFiltro").attr("uk-spinner", "");
				},
				success: function(resposta){
					$("#recebeFiltro").removeAttr("uk-spinner");
					$("#recebeFiltro").html(resposta);
				}
			})
	})
}

function mudarFiltro() {
	if($('#filtro').val()=="titulo"){
		$('#tituloField').show();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}else if($('#filtro').val()=="valor") {
		$('#tituloField').hide();
		$('#valorField').show();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}else if($('#filtro').val()=="tipoPagamento") {
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').show();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}
	else if($('#filtro').val()=="tipoRegistro") {
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').show();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}
	else if($('#filtro').val()=="dataOperacao") {
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').show();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}
	else if($('#filtro').val()=="dataRegistro") {
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').show();
		$('#dataFieldV').hide();
	}else if($('#filtro').val()=="dataVencimento") {
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').show();
	}else if($("#filtro").val() == "nenhum"){
		window.location.href='registros.jsp';
	}else{
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}
}

function deletarConta(id){
	alertify.confirm("Tem certeza que quer apagar esta conta?","Fazendo isso irá perder os dados de sua conta aqui no yourMoney.", function(){
		$.ajax({
			url:"apagarConta?id="+id,
			method: "GET",
			success:function(response){
				if(response.codigo == 1){
					alertify.success(response.mensagem);
					$.ajax({
						url: "contaAjax?acao=mostrar",
						method: "GET",
						success: function (response){
							
							$("#mostrarContas").html(response);
						}
					});	
				}else{
					alertify.error(response.mensagem);
				}
			}
		});

	}, function(){});
}
function deletarCartao(id){
	alertify.confirm("Tem certeza que quer apagar este cartão?","Fazendo isso irá perder os dados do seu cartão aqui no yourMoney.", function(){
		$.ajax({
			url:"apagarCartao?id="+id,
			method: "GET",
			success:function(response){
				if(response.codigo == 1){
					alertify.success(response.mensagem);
					$.ajax({
						url: "cartaoAjax?acao=mostrar",
						method: "GET",
						success: function (response){
							if(response != ""){
								$("#mostrarCartao").html(response);
							}else{
								$("#mostrarCartao").html("<h1>Sem cartão!</h1>");
							}
						}
					});	
			}else{
				alertify.error(response.mensagem);
			}
			}
		});

	}, function(){});
}


function deletarRegistro(id,tipoPagamento,valor,id_forma){
	alertify.confirm("Tem certeza que deseja remover este registro?","Ao fazer isso você irá perder os dados do seu registro aqui no yourMoney.", function(){
		$.ajax({
			url:"apagarRegistro?id="+id+"&tipoPagamento="+tipoPagamento+"&valor="+valor+"&id_forma="+id_forma,
			method: "GET",
			success:function(response){
				if(response.codigo == 1){
					alertify.success("Seu registro foi removido com sucesso!");
					window.location.href="registros.jsp";
				}else{
					alertify.error(response.mensagem);
				}
			}
		});

	}, function(){});
}

function verRegistro(ind){
	var regDetalhes = detalhes[ind];
	alertify.myAlert(regDetalhes).resizeTo('40%','80%').setHeader('');
}





function mostrarAvisos(){
	alertify.myAlert(avisos).resizeTo('40%','80%').setHeader('Avisos');	
}
function maskValor(v) {
	$("#" + v).maskMoney({
		prefix : "R$ ",
		decimal : ",",
		thousands : ".",
		allowZero : true
	});
}
function maskConta(){
	var im = new Inputmask("9999999-9",{
		placeholder: "0"
	});
	im.mask($("#Nconta"));
}
function maskCartao(cartao) {
	var ina = new Inputmask("9999 9999 9999 9999", {
		placeholder : "0"
	});
	ina.mask(cartao);
}

function verificarVl(valor) {
	valor = $("#valor3").maskMoney("unmasked")[0];
	limite = $("#limite").maskMoney("unmasked")[0];
	if (!Number.isNaN(valor) && !Number.isNaN(limite)) {
		if (valor > limite) {
			$("#valor3").val("R$ 0,00");
		}
	}

}
function verificarLimite() {
	if($('#apenasDebito').prop('checked')){
		$('#limite').val("");
		document.getElementById("limite").setAttribute("disabled", "");
		$("#semConta").wrap('<span/>');
	}else{
		$('#limite').removeAttr("disabled");
		$("#semConta").unwrap();
	}
}

//carteira
function ativaEvento() {
	$("#carteira").submit(function(e) {
		e.preventDefault();
		if ($("#valor5").val() != "") {
			var valor = $("#valor5").maskMoney("unmasked")[0];
			if (valor < 0) {
				$("#spanVal").fadeIn("fast", function() {
					$("#spanVal").html("valor não pode ser nagativo.");
				});
				$("#spanVal").fadeOut(5000);
			} else {
				$.ajax({
					url : "carteira",
					data : {
						carteira: valor
					},
					method : "POST",
					dataType : "json",
					beforeSend : function() {
						$("#adicionarCarteira").html("");
						$("#adicionarCarteira").attr("disabled","");
						$("#adicionarCarteira").attr("uk-spinner","");
					},
					success : function(response) {
						$("#adicionarCarteira").html("adicionar");
						$("#adicionarCarteira").removeAttr("disabled");
						$("#adicionarCarteira").removeAttr("uk-spinner");
						if (response.codigo == 1) {
							alertify.success(response.mensagem);
							//atualizar carteira
							$.ajax({
								url: "carteiraAjax",
								method: "GET",
								success: function(response){
									window.location.href="dashboard.jsp" +
											"";
									//$("#carteiraDash").html(response);
								}
							});
						}else{
							alertify.error(response.mensagem);
						}
					},
					error: function() {
						$("#adicionarCarteira").html("adicionar");
						$("#adicionarCarteira").removeAttr("disabled");
						$("#adicionarCarteira").removeAttr("uk-spinner");
						alertify.error("ERRO NO SERVIDOR!");
					}

				});
			}
		} else {
			$("#spanVal").fadeIn("fast", function() {
				$("#spanVal").html("coloque algum valor.");
			});
			$("#spanVal").fadeOut(5000);
		}
	});
}

$(window).ready(function(){
	if(!alertify.myAlert){
		alertify.dialog('myAlert',function(){
		return{
			main:function(message){
		        this.message = message;
		      },
		      setup:function(){
		          return { 
		            focus: { element:0 },
	                options:{
	                    maximizable:false,
	                    resizable:true,
	                    movable:false
	                }
		          };
		      },
		      prepare:function(){
		        this.setContent(this.message);
		      }
		  }});
		}
	$("#carregandoConta")
		.removeAttr("uk-spinner")
		.toggleClass("uk-spinner");
	$.ajax({
		url: "contaAjax?acao=contagem",
		success: function(response){			
			if(response == 0){
				$("#NtemConta").show(1000);
				$(document).on("click",".botaoConta",function(){
					alertify.myAlert(formConta).resizeTo('40%','80%').setHeader('<h2>Adicionar Conta Bancária</h2>');
					adicionarConta("dashboard");
				});
			}else{
				$("#temConta").show(1000, function(){
					$(document).on("click",".botaoConta",function(){
						alertify.myAlert(formConta).resizeTo('40%','80%').setHeader('<h2>Adicionar Conta Bancária</h2>');
						adicionarConta("dashboard");
					});
				});
				$("#numeroDeContas").html(response);
			}
		}
	});
	$("#carregandoCartao")
	.removeAttr("uk-spinner")
	.toggleClass("uk-spinner");
	$.ajax({
		url: "cartaoAjax?acao=contagem",
		success: function(response){
			if(response == 0){
				$("#NtemCartao").show(1000);
				$(document).on("click",".botaoCartao",function(){
					alertify.myAlert(formCartao).resizeTo('40%','80%').setHeader('<h2>Adicionar Cartão de Crédito</h2>');
					maskCartao(document.getElementById('Ncartao'))
					adicionarCartao("dashboard");
				});
			}else{
				$("#temCartao").show(1000, function(){
					$(document).on("click",".botaoCartao",function(){
						alertify.myAlert(formCartao).resizeTo('40%','80%').setHeader('<h2>Adicionar Cartão de Crédito</h2>');
						maskCartao(document.getElementById('Ncartao'))
						adicionarCartao("dashboard");
					});
				});
				$("#numeroDeCartao").html(response);
				
			}
		}
	});

	var botaoCarteira = document.getElementById("botaoCarteira");
	botaoCarteira.onclick = function(){
		alertify.myAlert(formCarteira).resizeTo('40%','40%').setHeader('<h2>Adicionar carteira</h2>');
		ativaEvento();
	}
	
	//carteira
	valor = (valor).toLocaleString('pt-BR', {
		style : 'currency',
		currency : "BRL"
	});
	$("#carteiraDash").html(valor);
});
function desmascarar(){
		var valor = $("#valor4").maskMoney("unmasked")[0];
		$("#valor4").val(valor);
}
