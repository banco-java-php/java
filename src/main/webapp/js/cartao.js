var cartoes = "";
var contasCartao = "";
$.ajax({
	url: "contaAjax?acao=selectContasCartao",
	success: function(response){
		contasCartao = response;
	}
});
$.ajax({
	url: "cartaoAjax?acao=selectCartao",
	success: function(response){
		cartoes = response;
	}
});
function verificarCartao() {
	var input = document.getElementById("Ncartao");
	var Ncartao = input.inputmask.unmaskedvalue();
	var contaCartao = $("#contaCartao").val();
	var resultado = false;
	if (Ncartao != "") {
			if (contaCartao != "") {
				resultado = true;
			} else {
				$("#avisosCartao").fadeIn("fast", function() {
					$("#avisosCartao").html("Selecione uma conta!");
				});
				$("#avisosCartao").fadeOut(10000);
			}
	}else {
		$("#avisosCartao").fadeIn("fast", function() {
			$("#avisosCartao").html("Coloque o numero do cartao!");
		});
		$("#avisosCartao").fadeOut(10000);
	} 
	return resultado;
}
function adicionarCartao(local){
	$("#contaCartao").html("<option value=''>Selecionar</option><option value='0' id='semConta'>Sem Conta Bancária</option>"+contasCartao);
	$("#cartaoForm").submit(function(e){
		e.preventDefault();
		if(verificarCartao()){
			var input = document.getElementById("Ncartao");
			var Ncartao = input.inputmask.unmaskedvalue();
			var limite = $("#limite").maskMoney("unmasked")[0];
			var contaCartao = $("#contaCartao").val();
			$.ajax({
				url : "AdicionarCartao",
				method : "POST",
				dataType : "JSON",
				data : {
					numero : Ncartao,
					conta : contaCartao,
					limite: limite
				},
				beforeSend: function() {
					$("#adicionarCartao").html("");
					$("#adicionarCartao").attr("disabled","");
					$("#adicionarCartao").attr("uk-spinner","");
				},
				error : function() {
					alertify.error("Erro no servidor!");
					$("#adicionarCartao").html("adicionar");
					$("#adicionarCartao").removeAttr("disabled");
					$("#adicionarCartao").removeAttr("uk-spinner");
				},
				success : function(resposta) {
					$("#adicionarCartao").html("adicionar");
					$("#adicionarCartao").removeAttr("disabled");
					$("#adicionarCartao").removeAttr("uk-spinner");
					if (resposta.codigo == 1) {
						$.ajax({
							url: "cartaoAjax?acao=selectCartao",
							success: function(response){
								cartoes = response;
							}
						});
						alertify.success(resposta.mensagem);
						document.forms[0].reset();
						if(local == "dashboard"){
							$.ajax({
								url: "cartaoAjax?acao=contagem",
								success: function(response){
									if(response != 0 && !$("#temCartao").is(":visible")){
										$("#NtemCartao").hide(1000);
										$("#temCartao").show(1000, function(){
											$(document).on("click",".botaoCartao", function(){
												alertify.myAlert(formCartao).resizeTo('40%','80%').setHeader('<h2>Adicionar cartão de crédito</h2>');
											});
										});
										$("#numeroDeCartao").html(response);
									}else{
										$("#numeroDeCartao").html(response);
									}
								}
							});
						}else{
							$.ajax({
								url: "cartaoAjax?acao=mostrar",
								method: "GET",
								success: function (response){
									if(response != ""){
										$("#mostrarCartao").html(response);
									}else{
										$("#mostrarCartao").html("<h1>Sem cartão!</h1>");
									}
								}
							});
						}
						
					}else{
						alertify.error(resposta.mensagem);
					}
				}

			});
		}
	});
}