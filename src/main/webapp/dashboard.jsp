<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Interface do Usuário</title>

		<link rel="stylesheet" type="text/css" href="css/uikit.min.css">
		<link rel="stylesheet" type="text/css" href="css/dashboard.css">
		<link rel="stylesheet" type="text/css" href="css/alertify.min.css">
		<link rel="stylesheet" href="css/skeuocard.reset.css" />
  		<link rel="stylesheet" href="css/skeuocard.css" />
	</head>
	<body>
	
	 
		<c:if test="${usuario == null}">
			<c:redirect url="login.jsp"></c:redirect>
		</c:if>
		<%@page import="java.util.Locale,java.text.NumberFormat,java.util.Collections,java.util.List,controle.UsuarioControle,java.util.concurrent.TimeUnit,java.util.Date,java.text.SimpleDateFormat,java.util.Calendar,modelo.Registro,controle.RegistroControle,
		modelo.Usuario,modelo.ContaBancaria,modelo.Cartao,controle.CartaoControle,
		controle.ContaBancariaControle,java.util.ArrayList" %>
			<% 
				Usuario user = (Usuario)request.getSession().getAttribute("usuario");	
				ArrayList<Registro> regList = new RegistroControle().consultarRegistros(user.getId());
				ArrayList<ContaBancaria> contaList = new ContaBancariaControle().consultarContasBancarias(user.getId());
				ArrayList<Cartao> cartaoList= new CartaoControle().consultarCartoes(user.getId());
				
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
				
				Date data = new Date();
			%>
		<header id="top-head" class="uk-position-fixed">
			<div class="uk-container uk-container-expand " style="background-color: #222;">
				<nav class="uk-navbar uk-light" data-uk-navbar="mode:click; duration: 250">
					<div class="uk-navbar-left">
						<img style='width:30px' src='imgs/money.png'> 
						<a href="dashboard.jsp">yourMoney</a>
					</div>
					<div class="uk-navbar-right">
						<ul class="uk-navbar-nav">
							<li><a data-uk-icon="icon:  bell" onclick="mostrarAvisos()" title="Avisos" data-uk-tooltip></a></li>
							<li><a href="Sair" data-uk-icon="icon:  sign-out" title="Sair" data-uk-tooltip></a></li>
							<li><a class="uk-navbar-toggle" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav" title="Menu" data-uk-tooltip></a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
		
		<div id="content" data-uk-height-viewport="expand: true">
			<div class="uk-container uk-container-expand">
				<div class="uk-grid uk-grid-divider uk-grid-medium uk-child-width-1-1 uk-child-width-1-4@l uk-child-width-1-5@xl" data-uk-grid>
					 <c:choose>
					 	<c:when test="${usuario.carteira == 0.00}">
						 	<div>
								<span class="uk-text-small"><span class="uk-margin-small-right uk-text-primary" style="font-size:17px">$</span>Carteira</span>
								<div class="uk-text-small">
									Você não possui.
								</div>
								<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button class="uk-button uk-button-secondary" id="botaoCarteira">Adicionar</button></h2>
							</div>	
					 	</c:when>
					 	<c:otherwise>
					 		<div>
								<span class="uk-text-small"><span class="uk-margin-small-right uk-text-primary" style="font-size:17px">$</span>Carteira</span>
								<div class="uk-heading-primary uk-margin-remove  uk-text-primary" style="font-size:150%;" id='carteiraDash'>
								</div>
								<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button class="uk-button uk-button-secondary" id="botaoCarteira">Adicionar</button></h2>
							</div>
					 	</c:otherwise>
					 </c:choose>
				 	<div id="carregandoConta" uk-spinner>
				 		<div id="NtemConta" style="display: none;">
							<span class="uk-text-small"><span data-uk-icon="icon:world" class="uk-margin-small-right uk-text-primary"></span>Contas</span>
							<div class="uk-text-small" >
								Você não possui.
							</div>
							<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button class="botaoConta uk-button uk-button-secondary">Adicionar</button></h2>
						</div>
						<div id="temConta" style="display: none;">
							<span class="uk-text-small"><span data-uk-icon="icon:world" class="uk-margin-small-right uk-text-primary"></span>Contas</span>
							<div class="uk-text-small" >
								Você possui <span id="numeroDeContas"></span> <a href='contas.jsp'>(Gerenciar)</a>
							</div>
							<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button class="botaoConta uk-button uk-button-secondary">Adicionar</button></h2>
						</div>
					</div>
					<div id="carregandoCartao" uk-spinner>
				 		<div id="NtemCartao" style="display: none;">
							<span class="uk-text-small"><span data-uk-icon="icon:world" class="uk-margin-small-right uk-text-primary"></span>Cartão de crédito</span>
							<div class="uk-text-small" >
								Você não possui.
							</div>
							<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button class="botaoCartao uk-button uk-button-secondary">Adicionar</button></h2>
						</div>
						<div id="temCartao" style="display: none;">
							<span class="uk-text-small"><span data-uk-icon="icon:world" class="uk-margin-small-right uk-text-primary"></span>Cartão de crédito</span>
							<div class="uk-text-small" >
								Você possui <span id="numeroDeCartao"></span> <a href='cartoes.jsp'>(Gerenciar)</a>
							</div>
							<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button class="botaoCartao uk-button uk-button-secondary">Adicionar</button></h2>
						</div>
					</div>
					
					<div>
						<span class="uk-text-small"><span data-uk-icon="icon:world" class="uk-margin-small-right uk-text-primary"></span>Registros</span>
						<div class="uk-text-small" >
							Registros
						</div>
						<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button id="botaoRegistro" class="uk-button uk-button-secondary">Adicionar</button></h2>
					</div>
					
				</div>
				<hr>
				<div class="uk-grid uk-grid-medium" data-uk-grid>
					<div class="uk-width-1-2@l">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Últimos registros <a href="registros.jsp">(Ver mais)</a></h4></div>
									
								</div>
							</div>
							<div class="uk-card-body uk-overflow-auto" style='max-height:500px'>
								<table class="uk-table uk-table-divider">
									<tr>
										<th></th>
										<th>Título</th>
										<th>Valor</th>
										<th>Operação</th>
										<th>Data</th>
									</tr>
									<tbody id="mostrarRegistros">
									<%
										int cont = 0;
										for(Registro rL : regList){
											if(cont <= 5){
												String titulo = rL.getTitulo();
												double valor = rL.getValor();
												Locale localeBR = new Locale( "pt", "BR" );  
												NumberFormat dinheiroBR = NumberFormat.getCurrencyInstance(localeBR);  
												String valorForm = dinheiroBR.format(rL.getValor()); 
												String tpRegistro = "plus.png";
												String tipoRegistro = "Entrada";
												if(rL.getTipoRegistro().equals("saida")){
													valor = valor*(-1);
													tpRegistro = "less.png";
													tipoRegistro = "Saída";
												}
												double valorNormal = valor;
	
												int idForma = user.getId();
												String meioDetalhado = "";
												String meio = "";
												if(rL.getTipoPagamento().equals("carteira")){
													meio = "Carteira";
													meioDetalhado = "Carteira";
												}else if(rL.getTipoPagamento().equals("conta")){
													meio = "Conta Bancaria";
													String conta = "Sem conta bancaria";
													if(rL.getConta().getConta() != 0){
														conta = Integer.toString(rL.getConta().getConta());
													}
													meioDetalhado = "Conta Bancaria "+rL.getConta().getConta()+" - "+rL.getConta().getAgencia().getNome();
													idForma = rL.getConta().getConta();
												}else{
													meio = "Cartão";
													if(rL.getCartao().getConta().getConta()==0){
														meioDetalhado = "Cartão "+rL.getCartao().getId()+" - "+rL.getCartao().getConta().getAgencia().getNome();
													}else{
														meioDetalhado = "Cartão "+rL.getCartao().getId()+" - Sem Banco";
													}
													
													idForma = rL.getCartao().getId();
												}
												
												Date dataReg = sdf1.parse(rL.getDataRegistro()); 
												Date dataOp = sdf1.parse(rL.getDataOperacao());
												String dataVen = "";
												if(rL.getDataVencimento()!=null){
													Date dtVen = sdf1.parse(rL.getDataVencimento());
													Date dtVen2 = sdf1.parse(rL.getDataVencimento());
													dataVen = "Dia de Vencimento: "+dtVen2+" <> Até "+dtVen;
												}else{
													dataVen = "";
												}
												Calendar dataRegCal = Calendar.getInstance();
												dataRegCal.setTime(dataReg);
												out.print("<tr>");
												out.print("<td><img style='width:15px;height:15px' src='imgs/"+tpRegistro+"'></td>");
												out.print("<td>"+titulo+"</td>");
												out.print("<td>"+valorForm+"</td>");
												out.print("<td>"+meio+"</td>");
												out.print("<td>"+sdf2.format(dataReg)+"</td>");
												out.print("</tr>");
												
											}else{
												break;
											}
											cont+=1;
											
											
										}
									%>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<div class="uk-width-1-2@l">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Sua Carteira este mês</h4></div>
								</div>
							</div>
							<div class="uk-card-body">
								
								<div class="chart-container">
									<canvas id="chart2"></canvas>
								</div>


							</div>
						</div>
					</div>

					<div class="uk-width-1-1 uk-width-1-3@l uk-width-1-2@xl">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Saída mensal</h4></div>
								</div>
							</div>
							<div class="uk-card-body">
								
								<div class="chart-container">
									<canvas id="chart3"></canvas>
								</div>


							</div>
						</div>
					</div>

					<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Entrada mensal</h4></div>
								</div>
							</div>
							<div class="uk-card-body">
								
								<div class="chart-container">
									<canvas id="chart4"></canvas>
								</div>
							

							</div>
						</div>
					</div>

					<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Balanço Geral - Mês</h4></div>
								</div>
							</div>
							<div class="uk-card-body">
								<div class="chart-container">
									<canvas id="chart5"></canvas>
								</div>
							</div>
						</div>
					</div>

				</div>
				<footer class="uk-section uk-section-small uk-text-center">
					<hr>
					<p class="uk-text-small uk-text-center">© 2019 yourMoney - <a href="">Desenvolvido por Lucas Silva, Lucas Wendel e Rodrigo Silva</a> | Feito com <a href="http://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip><span data-uk-icon="uikit"></span></a> </p>
				</footer>
			</div>
		</div>
		<div id="offcanvas-nav" data-uk-offcanvas="flip: true; overlay: true">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-slide">
				<button class="uk-offcanvas-close uk-close uk-icon" type="button" data-uk-close></button>
				<aside id="left-col" class="uk-light ">
					<div class="left-logo uk-flex uk-flex-middle">
						<img class="custom-logo" src="imgs/money.png" alt="yourMoney"> yourMoney
					</div>
					<div class="left-content-box  content-box-dark">
						<h4 class="uk-text-center uk-margin-remove-vertical text-light"></h4>
						
						<div class="uk-position-relative uk-text-center uk-display-block">
						    <a href="#" class="uk-text-small uk-text-muted uk-display-block uk-text-center" data-uk-icon="icon: triangle-down; ratio: 0.7"><%  out.print(user.getNome()); %></a>
						   
						    <div class="uk-dropdown user-drop" data-uk-dropdown="mode: click; pos: bottom-center; animation: uk-animation-slide-bottom-small; duration: 150">
						    	<ul class="uk-nav uk-dropdown-nav uk-text-left">
										<li><a href="Sair"><span data-uk-icon="icon: sign-out"></span> Sair</a></li>
							    </ul>
						    </div>
						   
						</div>
					</div>
					<div class="left-nav-wrap">
						<ul class="uk-nav uk-nav-default uk-nav-parent-icon" data-uk-nav>
							<li class="uk-nav-header">PÁGINAS</li>
							<li><a href="dashboard.jsp"><span data-uk-icon="icon: home" class="uk-margin-small-right"></span>Início</a></li>
							<li><a href="registros.jsp"><span data-uk-icon="icon: bookmark" class="uk-margin-small-right"></span>Registros</a></li>
							<li><a href="contas.jsp"><span data-uk-icon="icon: world" class="uk-margin-small-right"></span>Contas Bancárias</a></li>
							<li><a href="cartoes.jsp"><span data-uk-icon="icon: credit-card" class="uk-margin-small-right"></span>Cartões de Crédito</a></li>
							
						</ul>
						<div class="left-content-box uk-margin-top">
							
								<h5>Sobre o <i>yourMoney</i></h5>
								Com <i>yourMoney</i>, você pode administrar as suas transações, permitindo você facilitar a sua vida.
							
						</div>
						
					</div>
					
				</aside>
			</div>
		</div>
	
		<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
		<script src="js/uikit.min.js"></script>
		<script src="js/uikit-icons.min.js"></script>
		
		<script src="js/alertify.min.js"></script>
		<script type="text/javascript" src="js/input-mask.js"></script>
		<script type="text/javascript" src="js/maskMoney.min.js"></script>
		<%  
		String registrosFixos = "";
		for(int i=0;i<regList.size();i++){
			if(regList.get(i).getRepeticao().equals("fixa") && regList.get(i).getDataVencimento()!=null){
				int idReg = regList.get(i).getId();
				String titulo = regList.get(i).getTitulo();
				registrosFixos = registrosFixos + "<option value='"+idReg+"'>"+titulo+"</option>";
			}
		}

		String parcelamentos = "";
		for(int i=0;i<regList.size();i++){
			if(regList.get(i).getRepeticao().equals("parcelamento") && regList.get(i).getDataVencimento()!=null){
				int idReg = regList.get(i).getId();
				String titulo = regList.get(i).getTitulo();
				parcelamentos = parcelamentos + "<option value='"+idReg+"'>"+titulo+"</option>";
			}
		}
		
		//----------------------------------------------------
		SimpleDateFormat sdfMini = new SimpleDateFormat("m");
		SimpleDateFormat sdfMini2 = new SimpleDateFormat("Y");
		SimpleDateFormat sdfMini3 = new SimpleDateFormat("d");
		List<Double> carteiraData = new ArrayList<Double>();
		
		Calendar dataCal = Calendar.getInstance();
		dataCal.setTime(new Date());
		int diaHoje = dataCal.get(Calendar.DAY_OF_MONTH);
		int mesHoje = dataCal.get(Calendar.MONTH);
		int anoHoje = dataCal.get(Calendar.YEAR);
		UsuarioControle uC = new UsuarioControle();
		Usuario user2 = (Usuario) request.getSession().getAttribute("usuario");
		double val = user2.getCarteira(); 
		for(int i=diaHoje-2;i>=0;i--){

			for(int y=0;y<regList.size();y++){
				Calendar dt = Calendar.getInstance();
				Date dtDate = sdf1.parse(regList.get(y).getDataOperacao());
				dt.setTime(dtDate);
				int anoReg = dt.get(Calendar.YEAR);
				int diaReg = dt.get(Calendar.DAY_OF_MONTH);
				int mesReg = dt.get(Calendar.MONTH);
				 if((anoReg==anoHoje) && (mesReg==mesHoje) && (diaReg==(i+2)) && (regList.get(y).getTipoPagamento().equals("carteira"))){
					 if(regList.get(y).getTipoRegistro().equals("entrada")){
						val = val - regList.get(y).getValor();
					}else{
						val = val + regList.get(y).getValor();
					}  
				}  
			}

			carteiraData.add(val);
			
			
		}
		
		String carteiraDataString = "";
		for(int i=0;i<carteiraData.size();i++){
			carteiraDataString = carteiraDataString + "|" + carteiraData.get(i);
		}
		//
		double saidaTotalData0 = 0;
		double saidaTotalData1 = 0;
		double saidaTotalData2 = 0;
		double entradaTotalData0 = 0;
		double entradaTotalData1 = 0;
		double entradaTotalData2 = 0;
		for(Registro rL : regList) {
			Date dtDateReg = sdf1.parse(rL.getDataOperacao());
			Calendar dtReg = Calendar.getInstance();
			dtReg.setTime(dtDateReg);
			int anoReg2 = dtReg.get(Calendar.YEAR);
			int mesReg2 = dtReg.get(Calendar.MONTH);
			if((mesReg2==mesHoje) && (anoReg2==anoHoje)){
				if(rL.getTipoRegistro().equals("saida")){
					if(rL.getTipoPagamento().equals("carteira")){
						saidaTotalData0= saidaTotalData0 + rL.getValor();
					}else if(rL.getTipoPagamento().equals("conta")) {
						saidaTotalData1= saidaTotalData1 + rL.getValor();
					}else{
						saidaTotalData2= saidaTotalData2 + rL.getValor();
					}

				}else{
					if(rL.getTipoPagamento().equals("carteira")){
						entradaTotalData0= entradaTotalData0 + rL.getValor();
					}else if(rL.getTipoPagamento().equals("conta")) {
						entradaTotalData1= entradaTotalData1 + rL.getValor();
					}else{
						entradaTotalData2= entradaTotalData2 + rL.getValor();
					}

				}
			}
		}

		 

		//----------------------------------------------------
		String avisos = "<div class='uk-grid-small' uk-grid>";
		String avisosPrioridadeAlta = "";
		String avisosPrioridadeMedia = "";
		String avisosPrioridadeBaixa = "";
		int countAvisos = 0;
		for(Registro rL : regList) {
			if(rL.getDataVencimento()!=null){
				Calendar dtVenReg = Calendar.getInstance();
				if(rL.getRepeticao().equals("fixa")){
					dtVenReg.setTime(sdf1.parse(rL.getDataVencimento()));
				}else{
					Date dtVenR = sdf1.parse(rL.getDataOperacao());
					
					dtVenReg.setTime(dtVenR);
					dtVenReg.add(Calendar.MONTH, 1);
				}
				Date dtVenRegg = dtVenReg.getTime();
				long diffInMillies = Math.abs(dtVenRegg.getTime() - data.getTime());
			    long difDatas = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
			    Calendar dataCalendar = Calendar.getInstance();
			    dataCalendar.setTime(data);
			    Calendar dataHoje = Calendar.getInstance();
			    dataHoje.setTime(data);
			    boolean venMenorHoje = false;
			   	if(dataHoje.get(Calendar.DAY_OF_MONTH) > dtVenReg.get(Calendar.DAY_OF_MONTH)){
			   		venMenorHoje = true;
			   	}
				if(difDatas<7 && venMenorHoje && difDatas!=0){
					avisosPrioridadeAlta+= ""+
					"<div class='uk-width-1-1'>"+
						"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>"+
							"<div class='uk-card-header'>"+
								"<div class='uk-grid uk-grid-small'>"+
									"<div class='uk-width-auto'><h4>Pagamento Vencido: "+rL.getTitulo()+"</h4></div>"+
								"</div>"+
							"</div>"+
							"<div class='uk-card-body'>"+
								"Você possui um pagamento vencido referente ao dia <b>"+sdf2.format(dtVenRegg)+"</b> !"+
							"</div>"+
						"</div>"+
					"</div>";
					countAvisos++;
				}else if(difDatas<1 && dtVenReg.get(Calendar.DAY_OF_MONTH)!=(dataCalendar.get(Calendar.DAY_OF_MONTH)+1)){
					avisosPrioridadeMedia+= ""+
					"<div class='uk-width-1-1'>"+
						"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>"+
							"<div class='uk-card-header'>"+
								"<div class='uk-grid uk-grid-small'>"+
									"<div class='uk-width-auto'><h4>A vencer hoje: "+rL.getTitulo()+"</h4></div>"+
								"</div>"+
							"</div>"+
							"<div class='uk-card-body'>"+
								"Um pagamento vence hoje <b>"+sdf2.format(dtVenRegg)+"</b>! "+
							"</div>"+
						"</div>"+
					"</div>";
					countAvisos++;
				}else if(difDatas<7){
					avisosPrioridadeBaixa+= ""+
					"<div class='uk-width-1-1'>"+
						"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>"+
							"<div class='uk-card-header'>"+
								"<div class='uk-grid uk-grid-small'>"+
									"<div class='uk-width-auto'><h4>Vencimento próximo: "+rL.getTitulo()+"</h4></div>"+
								"</div>"+
							"</div>"+
							"<div class='uk-card-body'>"+
								"Um pagamento está próximo de seu vencimento, que está previsto para o dia <b>"+sdf2.format(dtVenRegg)+"</b>."+
							"</div>"+
						"</div>"+
					"</div>";
					countAvisos++;
				}
		
			}
		}
		if(countAvisos==0){
			avisos+="Nenhum aviso por agora! :)";
		}
		avisos+= avisosPrioridadeAlta + avisosPrioridadeMedia + avisosPrioridadeBaixa;
		avisos+= "</div>";
		
		
		%>
		<script src="js/contas.js"></script>
		<script src="js/cartao.js"></script>
		<script>	
			var parcelamentos = "<% out.print(parcelamentos); %>";
			var registrosFixos = "<% out.print(registrosFixos); %>";
			
			var cData = "<% out.print(carteiraDataString); %>";
			var carteiraData = cData.split("|");
			carteiraData[0] = ${usuario.carteira};
			carteiraData.reverse();
			
			var labels = [];
			for(i=0;i<carteiraData.length;i++){
				if(i==0){
					labels[i] = "dia 1";
				}else if(i==7){
					labels[i] = "dia 8";
				}else if(i==14){
					labels[i] = "dia 15";
				}else if(i==21){
					labels[i] = "dia 22";
				}else if(i==28){
					labels[i] = "dia 29";
				}else if(i==(carteiraData.length)-1) {
					labels[i] = "hoje "+(i+1);
				}else{
					labels[i] = "";
				}
			}
			var saidaTotalData0 = "<% out.print(saidaTotalData0); %>";
			var saidaTotalData1 = "<% out.print(saidaTotalData1); %>";
			var saidaTotalData2 = "<% out.print(saidaTotalData2); %>";
			var entradaTotalData0 = "<% out.print(entradaTotalData0); %>";
			var entradaTotalData1 = "<% out.print(entradaTotalData1); %>";
			var entradaTotalData2 = "<% out.print(entradaTotalData2); %>";

			var avisos = "<% out.print(avisos); %>";
			//cartao
			var valor = ${usuario.carteira};
		</script>
		<script src="js/Chart.min.js"></script>
		<script src="js/chartScripts.js"></script>
		
		<script src="js/addForms.js"></script>
		<script src="js/javinha.js"></script>
		<script>
		var botRegistro = document.getElementById("botaoRegistro");
		if(!alertify.myAlert){
			alertify.dialog('myAlert',function(){
			return{
				main:function(message){
			        this.message = message;
			      },
			      setup:function(){
			          return { 
			            focus: { element:0 },
		                options:{
		                    maximizable:false,
		                    resizable:true,
		                    movable:false
		                }
			          };
			      },
			      prepare:function(){
			        this.setContent(this.message);
			      }
			  }});
			}
		botRegistro.onclick = function(){
			alertify.myAlert(formRegistro).resizeTo('40%','80%').setHeader('<h2>Adicionar registro</h2>');
		}
		</script>
		
	</body>
</html>
