<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Minhas Contas Bancárias</title>
		<link rel="icon" href="imgs/money.png">
		<link rel="stylesheet" type="text/css" href="css/uikit.min.css">
		<link rel="stylesheet" type="text/css" href="css/dashboard.css">
		<link rel="stylesheet" type="text/css" href="css/alertify.min.css">
	</head>
	<body>
		<c:if test="${usuario == null}">
			<c:redirect url="login.jsp"></c:redirect>
		</c:if>	
		<%@page import="modelo.Registro,controle.RegistroControle,
		modelo.Usuario,modelo.ContaBancaria,modelo.Cartao,controle.CartaoControle,
		controle.ContaBancariaControle,java.util.ArrayList, java.text.SimpleDateFormat, java.util.Date, java.util.concurrent.TimeUnit" %>
			<% 
				Usuario user = (Usuario)request.getSession().getAttribute("usuario");	
				ArrayList<Registro> regList = new RegistroControle().consultarRegistros(user.getId());
				ArrayList<ContaBancaria> contaList = new ContaBancariaControle().consultarContasBancarias(user.getId());
				ArrayList<Cartao> cartaoList= new CartaoControle().consultarCartoes(user.getId()); 
				SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
				Date data = new Date();
			%>
		<header id="top-head" class="uk-position-fixed">
			<div class="uk-container uk-container-expand " style="background-color: #222;">
				<nav class="uk-navbar uk-light" data-uk-navbar="mode:click; duration: 250">
					<div class="uk-navbar-left">
						<img style='width:30px' src='imgs/money.png'> 
						<a href="dashboard.jsp">yourMoney</a>
					</div>
					<div class="uk-navbar-right">
						<ul class="uk-navbar-nav">
							<li><a data-uk-icon="icon:  bell" onclick="mostrarAvisos()" title="Avisos" data-uk-tooltip></a></li>
							<li><a href="Sair" data-uk-icon="icon:  sign-out" title="Sair" data-uk-tooltip></a></li>
							<li><a class="uk-navbar-toggle" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav" title="Menu" data-uk-tooltip></a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
		
		<div id="content" data-uk-height-viewport="expand: true">
			<div class="uk-container uk-container-expand">
				<div class="uk-grid uk-grid-divider uk-grid-medium uk-child-width-1-1 uk-child-width-1-4@l uk-child-width-1-5@xl" data-uk-grid>
 					<c:choose>
					 	<c:when test="${usuario.carteira == 0.00}">
						 	<div>
								<span class="uk-text-small"><span class="uk-margin-small-right uk-text-primary" style="font-size:17px">$</span>Carteira</span>
								<div class="uk-text-small">
									Você não possui.
								</div>
								<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button class="uk-button uk-button-secondary" id="botaoCarteira">Adicionar</button></h2>
							</div>	
					 	</c:when>
					 	<c:otherwise>
					 		<div>
								<span class="uk-text-small"><span class="uk-margin-small-right uk-text-primary" style="font-size:17px">$</span>Carteira</span>
								<div class="uk-heading-primary uk-margin-remove  uk-text-primary" style="font-size:150%;" id='carteiraDash'>
								</div>
								<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button class="uk-button uk-button-secondary" id="botaoCarteira">Adicionar</button></h2>
							</div>
					 	</c:otherwise>
					 </c:choose>
					<div>
						<span class="uk-text-small"><span data-uk-icon="icon:world" class="uk-margin-small-right uk-text-primary"></span>Contas</span>
						
						<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button id="botaoConta" class="uk-button uk-button-secondary">Adicionar</button></h2>
					</div>
					
				</div>
				<hr>
				<div class="uk-grid uk-grid-medium" data-uk-grid id="mostrarContas">
					
		
				</div>
				<footer class="uk-section uk-section-small uk-text-center">
					<hr>
					<p class="uk-text-small uk-text-center">© 2019 yourMoney - <a href="">Desenvolvido por Lucas Silva, Lucas Wendel e Rodrigo Silva</a> | Feito com <a href="http://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip><span data-uk-icon="uikit"></span></a> </p>
				</footer>
			</div>
		</div>
		<div id="offcanvas-nav" data-uk-offcanvas="flip: true; overlay: true">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-slide">
				<button class="uk-offcanvas-close uk-close uk-icon" type="button" data-uk-close></button>
				<aside id="left-col" class="uk-light ">
					<div class="left-logo uk-flex uk-flex-middle">
						<img class="custom-logo" src="imgs/money.png" alt="yourMoney"> yourMoney
					</div>
					<div class="left-content-box  content-box-dark">
						<h4 class="uk-text-center uk-margin-remove-vertical text-light"></h4>
						
						<div class="uk-position-relative uk-text-center uk-display-block">
						    <a href="#" class="uk-text-small uk-text-muted uk-display-block uk-text-center" data-uk-icon="icon: triangle-down; ratio: 0.7"><%  out.print(user.getNome()); %></a>
						   
						    <div class="uk-dropdown user-drop" data-uk-dropdown="mode: click; pos: bottom-center; animation: uk-animation-slide-bottom-small; duration: 150">
						    	<ul class="uk-nav uk-dropdown-nav uk-text-left">
										<li><a href="Sair"><span data-uk-icon="icon: sign-out"></span> Sair</a></li>
							    </ul>
						    </div>
						   
						</div>
					</div>
					<div class="left-nav-wrap">
						<ul class="uk-nav uk-nav-default uk-nav-parent-icon" data-uk-nav>
							<li class="uk-nav-header">PÁGINAS</li>
							<li><a href="dashboard.jsp"><span data-uk-icon="icon: home" class="uk-margin-small-right"></span>Início</a></li>
							<li><a href="registros.jsp"><span data-uk-icon="icon: bookmark" class="uk-margin-small-right"></span>Registros</a></li>
							<li><a href="contas.jsp"><span data-uk-icon="icon: world" class="uk-margin-small-right"></span>Contas Bancárias</a></li>
							<li><a href="cartoes.jsp"><span data-uk-icon="icon: credit-card" class="uk-margin-small-right"></span>Cartões de Crédito</a></li>
							
						</ul>
						<div class="left-content-box uk-margin-top">
							
								<h5>Sobre o <i>yourMoney</i></h5>
								Com <i>yourMoney</i>, você pode administrar as suas transações, permitindo você facilitar a sua vida.
							
						</div>
						
					</div>
					
				</aside>
			</div>
		</div>
		
		
		<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
		<script src="js/uikit.min.js"></script>
		<script src="js/uikit-icons.min.js"></script>

		<script src="js/alertify.min.js"></script>
		<script type="text/javascript" src="js/input-mask.js"></script>
		<script type="text/javascript" src="js/maskMoney.min.js"></script>
		
			<% 
			
			String avisos = "<div class='uk-grid-small' uk-grid>";
			String avisosPrioridadeAlta = "";
			String avisosPrioridadeMedia = "";
			String avisosPrioridadeBaixa = "";
			int countAvisos = 0;
			for(Registro rL : regList) {
				if(rL.getDataVencimento()!=null){
					Calendar dtVenReg = Calendar.getInstance();
					if(rL.getRepeticao().equals("fixa")){
						dtVenReg.setTime(sdf1.parse(rL.getDataVencimento()));
					}else{
						Date dtVenR = sdf1.parse(rL.getDataOperacao());
						
						dtVenReg.setTime(dtVenR);
						dtVenReg.add(Calendar.MONTH, 1);
					}
					Date dtVenRegg = dtVenReg.getTime();
					long diffInMillies = Math.abs(dtVenRegg.getTime() - data.getTime());
				    long difDatas = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
				    Calendar dataCalendar = Calendar.getInstance();
				    dataCalendar.setTime(data);
				    Calendar dataHoje = Calendar.getInstance();
				    dataHoje.setTime(data);
				    boolean venMenorHoje = false;
				   	if(dataHoje.get(Calendar.DAY_OF_MONTH) > dtVenReg.get(Calendar.DAY_OF_MONTH)){
				   		venMenorHoje = true;
				   	}
					if(difDatas<0 && venMenorHoje && difDatas!=0){
						avisosPrioridadeAlta+= ""+
						"<div class='uk-width-1-1'>"+
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>"+
								"<div class='uk-card-header'>"+
									"<div class='uk-grid uk-grid-small'>"+
										"<div class='uk-width-auto'><h4>Pagamento Vencido: "+rL.getTitulo()+"</h4></div>"+
									"</div>"+
								"</div>"+
								"<div class='uk-card-body'>"+
									"Você possui um pagamento vencido referente ao dia <b>"+sdf1.format(dtVenRegg)+"</b> !"+
								"</div>"+
							"</div>"+
						"</div>";
						countAvisos++;
					}else if(difDatas<1 && dtVenReg.get(Calendar.DAY_OF_MONTH)!=(dataCalendar.get(Calendar.DAY_OF_MONTH)+1)){
						avisosPrioridadeMedia+= ""+
						"<div class='uk-width-1-1'>"+
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>"+
								"<div class='uk-card-header'>"+
									"<div class='uk-grid uk-grid-small'>"+
										"<div class='uk-width-auto'><h4>A vencer hoje: "+rL.getTitulo()+"</h4></div>"+
									"</div>"+
								"</div>"+
								"<div class='uk-card-body'>"+
									"Um pagamento vence hoje <b>"+sdf1.format(dtVenRegg)+"</b>! "+
								"</div>"+
							"</div>"+
						"</div>";
						countAvisos++;
					}else if(difDatas<7){
						avisosPrioridadeBaixa+= ""+
						"<div class='uk-width-1-1'>"+
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>"+
								"<div class='uk-card-header'>"+
									"<div class='uk-grid uk-grid-small'>"+
										"<div class='uk-width-auto'><h4>Vencimento próximo: "+rL.getTitulo()+"</h4></div>"+
									"</div>"+
								"</div>"+
								"<div class='uk-card-body'>"+
									"Um pagamento está próximo de seu vencimento, que está previsto para o dia <b>"+sdf1.format(dtVenRegg)+"</b>."+
								"</div>"+
							"</div>"+
						"</div>";
						countAvisos++;
					}
			
				}
			}
			if(countAvisos==0){
				avisos+="Nenhum aviso por agora! :)";
			}
			avisos+= avisosPrioridadeAlta + avisosPrioridadeMedia + avisosPrioridadeBaixa;
			avisos+= "</div>";
			%>
		<script>
		var detalhesString = "<% out.print(request.getAttribute("detalhes")); %>";
		var detalhes = detalhesString.split("|");

		var avisos = "<% out.print(avisos); %>";

		var contas = "";
		var cartoes = "";
		var contasCartao = "";
		var parcelamentos = "";
		var registrosFixos = "";
		var valor = ${usuario.carteira};
		$.ajax({
			url: "contaAjax?acao=mostrar",
			method: "GET",
			success: function (response){
				if(response != ""){
					$("#mostrarContas").html(response);
				}else{
					$("#mostrarContas").html("<h1 class='uk-text-center'>Você não tem conta registrada!</h1>");
				}
			}
		});
		var botConta = document.getElementById("botaoConta");
		if(!alertify.myAlert){
			alertify.dialog('myAlert',function(){
			return{
				main:function(message){
			        this.message = message;
			      },
			      setup:function(){
			          return { 
			            focus: { element:0 },
		                options:{
		                    maximizable:false,
		                    resizable:true,
		                    movable:false
		                }
			          };
			      },
			      prepare:function(){
			        this.setContent(this.message);
			      }
			  }});
			}
		
		botConta.onclick = function(){
			alertify.myAlert(formConta).resizeTo('40%','80%').setHeader('<h2>Adicionar Conta Bancária</h2>');
			adicionarConta("contas");
		}
		</script>
		<script src="js/addForms.js"></script>
		<script src="js/javinha.js"></script>
		<script src="js/contas.js"></script>
		
	</body>
</html>
