<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.Locale,java.text.NumberFormat,java.util.List,java.util.concurrent.TimeUnit,java.util.Date,java.text.SimpleDateFormat,java.util.Calendar,java.util.ArrayList,modelo.Usuario, modelo.Registro, modelo.ContaBancaria, modelo.Cartao, modelo.Usuario,controle.ContaBancariaControle, controle.CartaoControle, controle.RegistroControle,controle.UsuarioControle" %> 
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Meus registros</title>
		<link rel="icon" href="imgs/money.png">
		<link rel="stylesheet" type="text/css" href="css/uikit.min.css">
		<link rel="stylesheet" type="text/css" href="css/dashboard.css">
		<link rel="stylesheet" type="text/css" href="css/alertify.min.css">
		
	</head>
	<body>
	
	 	
			<c:if test="${usuario == null}">
				<c:redirect url="dashboard.jsp"></c:redirect>
			</c:if>
			<% 
				Usuario user = (Usuario)request.getSession().getAttribute("usuario");	
				ArrayList<Registro> regList = new RegistroControle().consultarRegistros(user.getId());
				
				ArrayList<ContaBancaria> contaList = new ContaBancariaControle().consultarContasBancarias(user.getId());
				ArrayList<Cartao> cartaoList= new CartaoControle().consultarCartoes(user.getId()); 
				
				List<String> detalhes = new ArrayList<String>();
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
				SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM");
				SimpleDateFormat sdf4 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				
				Date data = new Date();
			%>
		<header id="top-head" class="uk-position-fixed">
			<div class="uk-container uk-container-expand " style="background-color: #222;">
				<nav class="uk-navbar uk-light" data-uk-navbar="mode:click; duration: 250">
					<div class="uk-navbar-left">
						<img style='width:30px' src='imgs/money.png'>
						<a href="dashboard.jsp">yourMoney</a>
					</div>
					<div class="uk-navbar-right">
						<ul class="uk-navbar-nav">
							<li><a data-uk-icon="icon:  bell" onclick="mostrarAvisos()" title="Avisos" data-uk-tooltip></a></li>
							<li><a href="Sair" data-uk-icon="icon:  sign-out" title="Sair" data-uk-tooltip></a></li>
							<li><a class="uk-navbar-toggle" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav" title="Offcanvas" data-uk-tooltip></a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
		
		<div id="content" data-uk-height-viewport="expand: true">
			<div class="uk-container uk-container-expand">
				<div class="uk-grid uk-grid-divider uk-grid-medium uk-child-width-1-1 uk-child-width-1-4@l uk-child-width-1-5@xl" data-uk-grid>
					<div>
						
					<c:choose>
					 	<c:when test="${usuario.carteira == 0.00}">
						 	<div>
								<span class="uk-text-small"><span class="uk-margin-small-right uk-text-primary" style="font-size:17px">$</span>Carteira</span>
								<div class="uk-text-small">
									Você não possui.
								</div>
								<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button class="uk-button uk-button-secondary" id="botaoCarteira">Adicionar</button></h2>
							</div>	
					 	</c:when>
					 	<c:otherwise>
					 		<div>
								<span class="uk-text-small"><span class="uk-margin-small-right uk-text-primary" style="font-size:17px">$</span>Carteira</span>
								<div class="uk-heading-primary uk-margin-remove  uk-text-primary" style="font-size:150%;" id='carteiraDash'>
								</div>
								<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button class="uk-button uk-button-secondary" id="botaoCarteira">Adicionar</button></h2>
							</div>
					 	</c:otherwise>
					 </c:choose>	

					</div>
						
					<div>
						<span class="uk-text-small"><span data-uk-icon="icon:bookmark" class="uk-margin-small-right uk-text-primary"></span>Registros</span>
						
						<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button id="botaoRegistro" class="uk-button uk-button-secondary">Adicionar</button></h2>
					</div>
					
				</div>
				<hr>
				<div class="uk-grid uk-grid-medium" data-uk-grid>
					
					<div class="uk-width-2-3@l">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Meus registros</h4></div>
									
								</div>
							</div>
							<div class="uk-card-body uk-overflow-auto" style='max-height:500px'>
							
								
								<table class="uk-table uk-table-divider uk-table-striped uk-table-hover">
									<tr>
										<th></th>
										<th>Título</th>
										<th>Valor</th>
										<th>Meio</th>
										<th>Data</th>
										<th></th>
									</tr>
									<tbody id='recebeFiltro'>
									<%
										int cont = 0;
										for(Registro rL : regList){
											String titulo = rL.getTitulo();
											double valor = rL.getValor();
											Locale localeBR = new Locale( "pt", "BR" );  
											NumberFormat dinheiroBR = NumberFormat.getCurrencyInstance(localeBR);  
											String valorForm = dinheiroBR.format(rL.getValor()); 
											String tpRegistro = "plus.png";
											String tipoRegistro = "Entrada";
											if(rL.getTipoRegistro().equals("saida")){
												valor = valor*(-1);
												tpRegistro = "less.png";
												tipoRegistro = "Saída";
											}
											double valorNormal = valor;
											 
											
											int idForma = user.getId();
											String meioDetalhado = "";
											String meio = "";
											if(rL.getTipoPagamento().equals("carteira")){
												meio = "Carteira";
												meioDetalhado = "Carteira";
											}else if(rL.getTipoPagamento().equals("conta")){
												meio = "Conta Bancaria";
												meioDetalhado = "Conta Bancaria "+rL.getConta().getConta()+" - "+rL.getConta().getAgencia().getNome();
												idForma = rL.getConta().getConta();
											}else{
												meio = "Cartão";
												if(rL.getCartao().getConta().getConta()==0){
													meioDetalhado = "Cartão "+rL.getCartao().getId()+" - "+rL.getCartao().getConta().getAgencia().getNome();
												}else{
													meioDetalhado = "Cartão "+rL.getCartao().getId()+" - Sem Banco";
												}
												
												idForma = rL.getCartao().getId();
											}
											
											Date dataReg = sdf1.parse(rL.getDataRegistro()); 
											Date dataOp = sdf.parse(rL.getDataOperacao());
											String dataVen = "";
											
											if(rL.getDataVencimento()!=null){
												Date dtVen = sdf.parse(rL.getDataVencimento());
												Calendar dataVenCal = Calendar.getInstance();
												dataVenCal.setTime(dtVen);
												dataVen = "Dia de Vencimento: "+sdf3.format(dtVen)+" <> Até "+sdf2.format(dtVen);
											}else{
												dataVen = "";
											}
											Calendar dataRegCal = Calendar.getInstance();
											dataRegCal.setTime(dataReg);
											Calendar dataOpCal = Calendar.getInstance();
											dataOpCal.setTime(dataOp);
											out.print("<tr>");
											out.print("<td><img style='width:15px;height:15px' src='imgs/"+tpRegistro+"'></td>");
											out.print("<td>"+titulo+"</td>");
											out.print("<td>"+valorForm+"</td>");
											out.print("<td>"+meio+"</td>");
											out.print("<td>"+sdf4.format(dataReg)+"</td>");
											out.print("<td><a class='uk-icon-button verRegistro' onclick='verRegistro("+cont+")'><span uk-icon='icon: search'></span></a>");
											out.print("<a onclick=\"deletarRegistro("+rL.getId()+",'"+rL.getTipoPagamento()+"',"+valor+","+idForma+")\" class='uk-icon-button'><span uk-icon='icon: close'></span></a></td>");
											out.print("</tr>");
											
											String deta = ""+
												"<h2>"+titulo+" <a class='uk-icon-button btnPDF' id='"+cont+"'><span uk-icon='icon: download'></span></a></h2>"+
												"<hr></hr>"+
												"<p>Valor: "+valorForm+"</span></p>"+
												"<p>Tipo de registro: "+tipoRegistro+"</p>"+
												"<p>Meio: "+meioDetalhado+"</p>"+
												"<p>Data de registro: "+sdf4.format(dataReg)+"</p>"+
												"<p>Data de operação: "+sdf2.format(dataOp)+"</p>"+
												"<p>"+dataVen+"</p>"+
												"<hr class='uk-divider-icon'></hr>"+
												"<p>"+rL.getDescricao()+"</p>";
												
											detalhes.add(deta);
													
											cont+=1;
											
										}
										String detalhesString = String.join("|", detalhes);
									%>
									</tbody>
								</table>
							</div>
						</div>
					</div>
			
					<div class="uk-width-1-3@l">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Filtros</h4></div>
								</div>
							</div>
							<div class="uk-card-body">
								<form id='filtroForm'>
									<label>Filtrar..</label>
									<select class="uk-select" id='filtro' name='filtro' onchange='mudarFiltro()'>
										<option value='nenhum'>Todos</option>
										<option value='titulo'>Por título</option>
										<option value='valor'>Por valor</option>
										<option value='tipoPagamento'>Por meio</option>
										<option value='tipoRegistro'>Por tipo de registro</option>
										<option value='dataOperacao'>Por data de operação</option>
										<option value='dataRegistro'>Por data de registro</option>
										<option value='dataVencimento'>Por data de vencimento</option>
									</select><br>
									<div id='tituloField' style='display:none'><label>Título</label><br><input class='uk-input' type='text' id='titulo' name='titulo'  placeholder='Título'></div>
									<div id='valorField' style='display:none'><label>Valor</label><br><input class='uk-input' type='text' id='valor' onclick="maskValor('valor')" name='valor' placeholder='Valor'></div>
									<div id="tipoFieldP" style='display:none'><label>Tipo de Pagamento</label><br><select class='uk-select'  id='tipoPagamento' name='tipoPagamento'><option value='carteira'>Carteira</option><option value='conta'>Conta Bancária</option><option value='cartao'>Cartão de Crédito</option></select></div>
									<div id="tipoFieldR" style='display:none'><label>Tipo de Registro</label><br><select class='uk-select' id='tipoRegistro' name='tipoRegistro'><option value='saida'>Saída</option><option value='entrada'>Entrada</option></select></div>
									<div id="dataFieldO" style='display:none'><label>Data de operação</label><br><input class='uk-input' type='date' id='dataOperacao' name='dataOperacao'></div>
									<div id="dataFieldR" style='display:none'><label>Data de Registro</label><br><input class='uk-input' type='date' id='dataRegistro' name='dataRegistro'></div>
									<div id="dataFieldV" style='display:none'><label>Data de Vencimento</label><br><input class='uk-input' type='date' id='dataVencimento' name='dataVencimento'></div><br>
									<input class='uk-button uk-button-secondary' type='submit' onclick='filtro()' value='Filtrar'>
								</form>
							</div>
						</div>
					</div>
				</div>
				
				</div>
				<footer class="uk-section uk-section-small uk-text-center">
					<hr>
					<p class="uk-text-small uk-text-center">ano 2019 yourMoney - <a href="">Desenvolvido por Lucas Silva, Lucas Wendel e Rodrigo Silva</a> | Feito com <a href="http://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip><span data-uk-icon="uikit"></span></a> </p>
				</footer>
			</div>
		<!-- </div> -->
		
		<div id="offcanvas-nav" data-uk-offcanvas="flip: true; overlay: true">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-slide">
				<button class="uk-offcanvas-close uk-close uk-icon" type="button" data-uk-close></button>
				<aside id="left-col" class="uk-light ">
					<div class="left-logo uk-flex uk-flex-middle">
						<img class="custom-logo" src="imgs/money.png" alt="yourMoney"> yourMoney
					</div>
					<div class="left-content-box  content-box-dark">
						<h4 class="uk-text-center uk-margin-remove-vertical text-light"></h4>
						
						<div class="uk-position-relative uk-text-center uk-display-block">
						    <a href="#" class="uk-text-small uk-text-muted uk-display-block uk-text-center" data-uk-icon="icon: triangle-down; ratio: 0.7"><%  out.print(user.getNome()); %></a>
						   
						    <div class="uk-dropdown user-drop" data-uk-dropdown="mode: click; pos: bottom-center; animation: uk-animation-slide-bottom-small; duration: 150">
						    	<ul class="uk-nav uk-dropdown-nav uk-text-left">
										<li><a href="Sair"><span data-uk-icon="icon: sign-out"></span> Sair</a></li>
							    </ul>
						    </div>
						   
						</div>
					</div>
					<div class="left-nav-wrap">
						<ul class="uk-nav uk-nav-default uk-nav-parent-icon" data-uk-nav>
							<li class="uk-nav-header">PÁGINAS</li>
							<li><a href="dashboard.jsp"><span data-uk-icon="icon: home" class="uk-margin-small-right"></span>Início</a></li>
							<li><a href="registros.jsp"><span data-uk-icon="icon: bookmark" class="uk-margin-small-right"></span>Registros</a></li>
							<li><a href="contas.jsp"><span data-uk-icon="icon: world" class="uk-margin-small-right"></span>Contas Bancárias</a></li>
							<li><a href="cartoes.jsp"><span data-uk-icon="icon: credit-card" class="uk-margin-small-right"></span>Cartões de Crédito</a></li>
							
						</ul>
						<div class="left-content-box uk-margin-top">
							
								<h5>Sobre o <i>yourMoney</i></h5>
								Com <i>yourMoney</i>, você pode administrar as suas transações, permitindo você facilitar a sua vida.
							
						</div>
						
					</div>
					
				</aside>
			</div>
		</div>
		<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
		<script src="js/uikit.min.js"></script>
		<script src="js/uikit-icons.min.js"></script>
		<script src="js/alertify.min.js"></script>
		<script type="text/javascript" src="js/input-mask.js"></script>
		<script type="text/javascript" src="js/maskMoney.min.js"></script>
		
		<%  
			String registrosFixos = "";
			for(int i=0;i<regList.size();i++){
				if(regList.get(i).getRepeticao().equals("fixa") && regList.get(i).getDataVencimento()!=null){
					int idReg = regList.get(i).getId();
					String titulo = regList.get(i).getTitulo();
					registrosFixos = registrosFixos + "<option value='"+idReg+"'>"+titulo+"</option>";
				}
			}
	
			String parcelamentos = "";
			for(int i=0;i<regList.size();i++){
				if(regList.get(i).getRepeticao().equals("parcelamento") && regList.get(i).getDataVencimento()!=null){
					int idReg = regList.get(i).getId();
					String titulo = regList.get(i).getTitulo();
					parcelamentos = parcelamentos + "<option value='"+idReg+"'>"+titulo+"</option>";
				}
			}
			
			String avisos = "<div class='uk-grid-small' uk-grid>";
			String avisosPrioridadeAlta = "";
			String avisosPrioridadeMedia = "";
			String avisosPrioridadeBaixa = "";
			int countAvisos = 0;
			for(Registro rL : regList) {
				if(rL.getDataVencimento()!=null){
					Calendar dtVenReg = Calendar.getInstance();
					if(rL.getRepeticao().equals("fixa")){
						dtVenReg.setTime(sdf.parse(rL.getDataVencimento()));
					}else{
						Date dtVenR = sdf.parse(rL.getDataOperacao());
						
						dtVenReg.setTime(dtVenR);
						dtVenReg.add(Calendar.MONTH, 1);
					}
					Date dtVenRegg = dtVenReg.getTime();
					long diffInMillies = Math.abs(dtVenRegg.getTime() - data.getTime());
				    long difDatas = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
				    Calendar dataCalendar = Calendar.getInstance();
				    dataCalendar.setTime(data);
				    Calendar dataHoje = Calendar.getInstance();
				    dataHoje.setTime(data);
				    boolean venMenorHoje = false;
				   	if(dataHoje.get(Calendar.DAY_OF_MONTH) > dtVenReg.get(Calendar.DAY_OF_MONTH)){
				   		venMenorHoje = true;
				   	}
					if(difDatas<7 && venMenorHoje && difDatas!=0){
						avisosPrioridadeAlta+= ""+
						"<div class='uk-width-1-1'>"+
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>"+
								"<div class='uk-card-header'>"+
									"<div class='uk-grid uk-grid-small'>"+
										"<div class='uk-width-auto'><h4>Pagamento Vencido: "+rL.getTitulo()+"</h4></div>"+
									"</div>"+
								"</div>"+
								"<div class='uk-card-body'>"+
									"Você possui um pagamento vencido referente ao dia <b>"+sdf2.format(dtVenRegg)+"</b> !"+
								"</div>"+
							"</div>"+
						"</div>";
						countAvisos++;
					}else if(difDatas<1 && dtVenReg.get(Calendar.DAY_OF_MONTH)!=(dataCalendar.get(Calendar.DAY_OF_MONTH)+1)){
						avisosPrioridadeMedia+= ""+
						"<div class='uk-width-1-1'>"+
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>"+
								"<div class='uk-card-header'>"+
									"<div class='uk-grid uk-grid-small'>"+
										"<div class='uk-width-auto'><h4>A vencer hoje: "+rL.getTitulo()+"</h4></div>"+
									"</div>"+
								"</div>"+
								"<div class='uk-card-body'>"+
									"Um pagamento vence hoje <b>"+sdf2.format(dtVenRegg)+"</b>! "+
								"</div>"+
							"</div>"+
						"</div>";
						countAvisos++;
					}else if(difDatas<7){
						avisosPrioridadeBaixa+= ""+
						"<div class='uk-width-1-1'>"+
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>"+
								"<div class='uk-card-header'>"+
									"<div class='uk-grid uk-grid-small'>"+
										"<div class='uk-width-auto'><h4>Vencimento próximo: "+rL.getTitulo()+"</h4></div>"+
									"</div>"+
								"</div>"+
								"<div class='uk-card-body'>"+
									"Um pagamento está próximo de seu vencimento, que está previsto para o dia <b>"+sdf2.format(dtVenRegg)+"</b>."+
								"</div>"+
							"</div>"+
						"</div>";
						countAvisos++;
					}
			
				}
			}
			if(countAvisos==0){
				avisos+="Nenhum aviso por agora! :)";
			}
			avisos+= avisosPrioridadeAlta + avisosPrioridadeMedia + avisosPrioridadeBaixa;
			avisos+= "</div>";
			
			
		%>
		<script src="js/cartao.js"></script>
		<script src="js/contas.js"></script>
		<script>
			var valor = ${usuario.carteira};
			var parcelamentos = "<% out.print(parcelamentos); %>";
			var registrosFixos = "<% out.print(registrosFixos); %>";
			var detalhesString = "<% out.print(detalhesString); %>";
			var detalhes = detalhesString.split("|");

			var avisos = "<% out.print(avisos); %>";
		</script>
		<script src="js/jspdf.min.js"></script>
		<script src="js/html2canvas.min.js"></script>
		<script type='text/javascript'>
			function savePDF(codigoHTML) {
			  var doc = new jsPDF('portrait', 'pt', 'a4');
			   var  data = new Date();
			  margins = {
			    top: 40,
			    bottom: 60,
			    left: 40,
			    width: 1000
			  };
			  doc.fromHTML(codigoHTML,
			               margins.left, // x coord
			               margins.top, { pagesplit: true },
			               function(dispose){
			    doc.save("registro - "+data.getDate()+"/"+(data.getMonth()+1)+"/"+data.getFullYear()+".pdf");
			  });
			}
			$('.verRegistro').on('click',function(){
		        $(".btnPDF").click(function(event) {
		        	var det = detalhes[$(this).attr('id')];
		        	var detalhesPDF = "<img src='imgs/money.png' width='40px'><h1>yourMoney</h1><hr>"+det;
		            savePDF(detalhesPDF);
		            alertify.closeAll();
		        });
		    });

		    
		</script>
		<script src="js/addForms.js"></script>
		<script src="js/javinha.js"></script>
		<script src="js/jspdf.min.js"></script>
		<script src="js/html2canvas.min.js"></script>
		<script type='text/javascript'>
			filtro();
			

		var botRegistro = document.getElementById("botaoRegistro");
		if(!alertify.myAlert){
			alertify.dialog('myAlert',function(){
			return{
				main:function(message){
			        this.message = message;
			      },
			      setup:function(){
			          return { 
			            focus: { element:0 },
		                options:{
		                    maximizable:false,
		                    resizable:true,
		                    movable:false
		                }
			          };
			      },
			      prepare:function(){
			        this.setContent(this.message);
			      }
			  }});
			}
		botRegistro.onclick = function(){
			alertify.myAlert(formRegistro).resizeTo('40%','80%').setHeader('<h2>Adicionar registro</h2>');
		}
		</script>
	</body>
</html>
